<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => 'This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>',
    'readme' => '#googleMap

DESCRIPTION

This output modifier accepts an address and returns a url for the static map image
```
PARAMETERS: 
w - default: 435
h - default: 300
z - default: 16
sensor (1 or 0) - default: 0
marker (1 or 0) - default: 1
key - Google Maps API v3 Key, note that static maps must be enabled

OUTPUT MODIFIER USAGE:
<img src="[[*tvAddress:googleMap=`w=425,h=300,z=16,key=xxxxxxxxxxx,sensor=0,marker=1`]]" />

SNIPPET USAGE:
<img src="[[googleMap? &address=`[[*tvAddress]]` &key=`xxxxxxxxxxx` &w=`425` &h=`300` &z=`16` &marker=`1` &sensor=`0`]]" />
```

`key` is the only REQUIRED variable, unless you manually create a system setting named `google_api_key`
then the snippet will use that value whenever a vlaue for \'key\' is not passed

AUTHOR: Jason Carney, DashMedia.com.au',
    'changelog' => '==googleMap 1.0.0-pl==
-inital release',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => '8f9d92bb87c10f0fda339444b8fc51ea',
      'native_key' => 'googlemap',
      'filename' => 'modNamespace/f4a2cab0e266197f9f8f219b2483435b.vehicle',
      'namespace' => 'googlemap',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '9d2925f1f35e07e9398e0f051c08ba0d',
      'native_key' => 1,
      'filename' => 'modCategory/b4e52b85ecfde46d7a0fee1d23f33042.vehicle',
      'namespace' => 'googlemap',
    ),
  ),
);