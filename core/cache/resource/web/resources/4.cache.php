<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 4,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Our services',
    'longtitle' => '',
    'description' => '',
    'alias' => 'who-are-you',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 3,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 2,
    'createdon' => 1410198435,
    'editedby' => 1,
    'editedon' => 1413373890,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1410198360,
    'publishedby' => 2,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => '',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'Left_content' => 
    array (
      0 => 'Left_content',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'richtext',
    ),
    'Right_content' => 
    array (
      0 => 'Right_content',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'richtext',
    ),
    'Heading_title' => 
    array (
      0 => 'Heading_title',
      1 => 'Getting better at what you do, everyday',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NGA</title>

    <!-- Bootstrap -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="about">
    	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         <ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li class="active"><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
         </div> </div>  
    <header id="main-header">
		<img id="header-banner" src="assets/images/bannerabout.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="index.php?id=8" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="http://nga.loyaltymatters.co.uk/"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             <ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li class="active"><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">Getting better at what you do, everyday</h1>
            </div>            
            <!-- Page Headline Start-->
        </div>
    </header>
    <!-- HEADER END->
	
    <!-- Breadcumb START-->
    <div class="block_breadcumb"> 
     <div class="container">
    	<div class="col-lg-12">
        	<ul class="B_crumbBox"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>
 > <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Our services" href="index.php?id=4"><span itemprop="title">Our services</span></a></li>
</ul>
        </div>
     </div>
    </div>    
    <!-- Breadcumb End-->

    <!-- General Page 2 Column content Start-->
	[[!If? &subject=`` &operator=`notempty` &then=`
    <div class="block_twocolumn"> 
    <div class="container">
        <div class="col-lg-6">
            

        </div>
        <div class="col-lg-6">
           
        </div>        
    </div>
    </div>
	`]]
    <!-- General Page 2 Column content End-->
    
	 <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->
   
	
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		<ul class="footer_nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li class="active"><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->
    <!-- jQuery (necessary for Bootstrap\'s JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Parallax with stellar.js-->
    <script src="/assets/js/parallax.js"></script>
	<script>
    $(function(){
        $.stellar({
            horizontalScrolling: false,
            verticalOffset: -100
        });
    });
    </script>    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/assets/js/bootstrap.min.js"></script>
  </body>
</html>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]' => '<ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li class="active"><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>',
    '[[~8]]' => 'index.php?id=8',
    '[[*Heading_title]]' => 'Getting better at what you do, everyday',
    '[[$Header]]' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         <ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li class="active"><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
         </div> </div>  
    <header id="main-header">
		<img id="header-banner" src="assets/images/bannerabout.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="index.php?id=8" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="http://nga.loyaltymatters.co.uk/"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             <ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li class="active"><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">Getting better at what you do, everyday</h1>
            </div>            
            <!-- Page Headline Start-->
        </div>
    </header>
    <!-- HEADER END->',
    '[[~4]]' => 'index.php?id=4',
    '[[$?resource=`4`&description=`Our services`&text=`Our services`]]' => '<a class="B_currentCrumb" itemprop="url" rel="Our services" href="index.php?id=4"><span itemprop="title">Our services</span></a>',
    '[[~1]]' => 'index.php?id=1',
    '[[$?description=`Home`&text=`Home`]]' => '<a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a>',
    '[[$?text=`<a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a>`]]' => '<li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>',
    '[[$?text=`<a class="B_currentCrumb" itemprop="url" rel="Our services" href="index.php?id=4"><span itemprop="title">Our services</span></a>`]]' => '<li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Our services" href="index.php?id=4"><span itemprop="title">Our services</span></a></li>',
    '[[$?text=`<li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>
 > <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Our services" href="index.php?id=4"><span itemprop="title">Our services</span></a></li>
`]]' => '<ul class="B_crumbBox"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>
 > <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Our services" href="index.php?id=4"><span itemprop="title">Our services</span></a></li>
</ul>',
    '[[Breadcrumbs? &crumbSeparator=`>`]]' => '<ul class="B_crumbBox"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>
 > <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Our services" href="index.php?id=4"><span itemprop="title">Our services</span></a></li>
</ul>',
    '[[*Left_content]]' => '',
    '[[*Right_content]]' => '',
    '[[$Services]]' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
    '[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]' => '<ul class="footer_nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li class="active"><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>',
    '[[$Footer]]' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		<ul class="footer_nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li class="active"><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'Header' => 
      array (
        'fields' => 
        array (
          'id' => 8,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Header',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
         </div> </div>  
    <header id="main-header">
		<img id="header-banner" src="assets/images/bannerabout.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="[[~8]]" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="[[++site_url]]"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">[[*Heading_title]]</h1>
            </div>            
            <!-- Page Headline Start-->
        </div>
    </header>
    <!-- HEADER END->',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
         </div> </div>  
    <header id="main-header">
		<img id="header-banner" src="assets/images/bannerabout.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="[[~8]]" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="[[++site_url]]"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">[[*Heading_title]]</h1>
            </div>            
            <!-- Page Headline Start-->
        </div>
    </header>
    <!-- HEADER END->',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'Services' => 
      array (
        'fields' => 
        array (
          'id' => 9,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Services',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'Footer' => 
      array (
        'fields' => 
        array (
          'id' => 7,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'Wayfinder' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Wayfinder',
          'description' => 'Wayfinder for MODx Revolution 2.0.0-beta-5 and later.',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * Wayfinder Snippet to build site navigation menus
 *
 * Totally refactored from original DropMenu nav builder to make it easier to
 * create custom navigation by using chunks as output templates. By using
 * templates, many of the paramaters are no longer needed for flexible output
 * including tables, unordered- or ordered-lists (ULs or OLs), definition lists
 * (DLs) or in any other format you desire.
 *
 * @version 2.1.1-beta5
 * @author Garry Nutting (collabpad.com)
 * @author Kyle Jaebker (muddydogpaws.com)
 * @author Ryan Thrash (modx.com)
 * @author Shaun McCormick (modx.com)
 * @author Jason Coward (modx.com)
 *
 * @example [[Wayfinder? &startId=`0`]]
 *
 * @var modX $modx
 * @var array $scriptProperties
 * 
 * @package wayfinder
 */
$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');

/* include a custom config file if specified */
if (isset($scriptProperties[\'config\'])) {
    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';
} else {
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';
}
if (file_exists($scriptProperties[\'config\'])) {
    include $scriptProperties[\'config\'];
}

/* include wayfinder class */
include_once $wayfinder_base.\'wayfinder.class.php\';
if (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {
    return \'error: Wayfinder class not found\';
}
$wf = new Wayfinder($modx,$scriptProperties);

/* get user class definitions
 * TODO: eventually move these into config parameters */
$wf->_css = array(
    \'first\' => isset($firstClass) ? $firstClass : \'\',
    \'last\' => isset($lastClass) ? $lastClass : \'last\',
    \'here\' => isset($hereClass) ? $hereClass : \'active\',
    \'parent\' => isset($parentClass) ? $parentClass : \'\',
    \'row\' => isset($rowClass) ? $rowClass : \'\',
    \'outer\' => isset($outerClass) ? $outerClass : \'\',
    \'inner\' => isset($innerClass) ? $innerClass : \'\',
    \'level\' => isset($levelClass) ? $levelClass: \'\',
    \'self\' => isset($selfClass) ? $selfClass : \'\',
    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'
);

/* get user templates
 * TODO: eventually move these into config parameters */
$wf->_templates = array(
    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',
    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',
    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',
    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',
    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',
    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',
    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',
    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',
    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',
    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',
    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'
);

/* process Wayfinder */
$output = $wf->run();
if ($wf->_config[\'debug\']) {
    $output .= $wf->renderDebugOutput();
}

/* output results */
if ($wf->_config[\'ph\']) {
    $modx->setPlaceholder($wf->_config[\'ph\'],$output);
} else {
    return $output;
}',
          'locked' => false,
          'properties' => 
          array (
            'level' => 
            array (
              'name' => 'level',
              'desc' => 'prop_wayfinder.level_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Depth (number of levels) to build the menu from. 0 goes through all levels.',
              'area' => '',
              'area_trans' => '',
            ),
            'includeDocs' => 
            array (
              'name' => 'includeDocs',
              'desc' => 'prop_wayfinder.includeDocs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Acts as a filter and will limit the output to only the documents specified in this parameter. The startId is still required.',
              'area' => '',
              'area_trans' => '',
            ),
            'excludeDocs' => 
            array (
              'name' => 'excludeDocs',
              'desc' => 'prop_wayfinder.excludeDocs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Acts as a filter and will remove the documents specified in this parameter from the output. The startId is still required.',
              'area' => '',
              'area_trans' => '',
            ),
            'contexts' => 
            array (
              'name' => 'contexts',
              'desc' => 'prop_wayfinder.contexts_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Specify the contexts for the Resources that will be loaded in this menu. Useful when used with startId at 0 to show all first-level items. Note: This will increase load times a bit, but if you set cacheResults to 1, that will offset the load time.',
              'area' => '',
              'area_trans' => '',
            ),
            'cacheResults' => 
            array (
              'name' => 'cacheResults',
              'desc' => 'prop_wayfinder.cacheResults_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Cache the generated menu to the MODX Resource cache. Setting this to 1 will speed up the loading of your menus.',
              'area' => '',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'prop_wayfinder.cacheTime_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 3600,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The number of seconds to store the cached menu, if cacheResults is 1. Set to 0 to store indefinitely until cache is manually cleared.',
              'area' => '',
              'area_trans' => '',
            ),
            'ph' => 
            array (
              'name' => 'ph',
              'desc' => 'prop_wayfinder.ph_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'To display send the output of Wayfinder to a placeholder set the ph parameter equal to the name of the desired placeholder. All output including the debugging (if on) will be sent to the placeholder specified.',
              'area' => '',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'prop_wayfinder.debug_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'With the debug parameter set to 1, Wayfinder will output information on how each Resource was processed.',
              'area' => '',
              'area_trans' => '',
            ),
            'ignoreHidden' => 
            array (
              'name' => 'ignoreHidden',
              'desc' => 'prop_wayfinder.ignoreHidden_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The ignoreHidden parameter allows Wayfinder to ignore the display in menu flag that can be set for each document. With this parameter set to 1, all Resources will be displayed regardless of the Display in Menu flag.',
              'area' => '',
              'area_trans' => '',
            ),
            'hideSubMenus' => 
            array (
              'name' => 'hideSubMenus',
              'desc' => 'prop_wayfinder.hideSubMenus_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The hideSubMenus parameter will remove all non-active submenus from the Wayfinder output if set to 1. This parameter only works if multiple levels are being displayed.',
              'area' => '',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'prop_wayfinder.useWeblinkUrl_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => ' If WebLinks are used in the output, Wayfinder will output the link specified in the WebLink instead of the normal MODx link. To use the standard display of WebLinks (like any other Resource) set this to 0.',
              'area' => '',
              'area_trans' => '',
            ),
            'fullLink' => 
            array (
              'name' => 'fullLink',
              'desc' => 'prop_wayfinder.fullLink_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'If set to 1, will display the entire, absolute URL in the link. (It is recommended to use scheme instead.)',
              'area' => '',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'prop_wayfinder.scheme_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'prop_wayfinder.relative',
                  'value' => '',
                  'name' => 'Relative',
                ),
                1 => 
                array (
                  'text' => 'prop_wayfinder.absolute',
                  'value' => 'abs',
                  'name' => 'Absolute',
                ),
                2 => 
                array (
                  'text' => 'prop_wayfinder.full',
                  'value' => 'full',
                  'name' => 'Full',
                ),
              ),
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Determines how URLs are generated for each link. Set to "abs" to show the absolute URL, "full" to show the full URL, and blank to use the relative URL. Defaults to relative.',
              'area' => '',
              'area_trans' => '',
            ),
            'sortOrder' => 
            array (
              'name' => 'sortOrder',
              'desc' => 'prop_wayfinder.sortOrder_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'prop_wayfinder.ascending',
                  'value' => 'ASC',
                  'name' => 'Ascending',
                ),
                1 => 
                array (
                  'text' => 'prop_wayfinder.descending',
                  'value' => 'DESC',
                  'name' => 'Descending',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Allows the menu to be sorted in either ascending or descending order.',
              'area' => '',
              'area_trans' => '',
            ),
            'sortBy' => 
            array (
              'name' => 'sortBy',
              'desc' => 'prop_wayfinder.sortBy_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'menuindex',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Sorts the output by any of the Resource fields on a level by level basis. This means that each submenu will be sorted independently of all other submenus at the same level. Random will sort the output differently every time the page is loaded if the snippet is called uncached.',
              'area' => '',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'prop_wayfinder.limit_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Causes Wayfinder to only process the number of items specified per level.',
              'area' => '',
              'area_trans' => '',
            ),
            'cssTpl' => 
            array (
              'name' => 'cssTpl',
              'desc' => 'prop_wayfinder.cssTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This parameter allows for a chunk containing a link to a style sheet or style information to be inserted into the head section of the generated page.',
              'area' => '',
              'area_trans' => '',
            ),
            'jsTpl' => 
            array (
              'name' => 'jsTpl',
              'desc' => 'prop_wayfinder.jsTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This parameter allows for a chunk containing some Javascript to be inserted into the head section of the generated page.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowIdPrefix' => 
            array (
              'name' => 'rowIdPrefix',
              'desc' => 'prop_wayfinder.rowIdPrefix_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'If set, Wayfinder will replace the id placeholder with a unique id consisting of the specified prefix plus the Resource id.',
              'area' => '',
              'area_trans' => '',
            ),
            'textOfLinks' => 
            array (
              'name' => 'textOfLinks',
              'desc' => 'prop_wayfinder.textOfLinks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'menutitle',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This field will be inserted into the linktext placeholder.',
              'area' => '',
              'area_trans' => '',
            ),
            'titleOfLinks' => 
            array (
              'name' => 'titleOfLinks',
              'desc' => 'prop_wayfinder.titleOfLinks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'pagetitle',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This field will be inserted into the linktitle placeholder.',
              'area' => '',
              'area_trans' => '',
            ),
            'displayStart' => 
            array (
              'name' => 'displayStart',
              'desc' => 'prop_wayfinder.displayStart_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Show the document as referenced by startId in the menu.',
              'area' => '',
              'area_trans' => '',
            ),
            'firstClass' => 
            array (
              'name' => 'firstClass',
              'desc' => 'prop_wayfinder.firstClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'first',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the first item at a given menu level.',
              'area' => '',
              'area_trans' => '',
            ),
            'lastClass' => 
            array (
              'name' => 'lastClass',
              'desc' => 'prop_wayfinder.lastClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'last',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the last item at a given menu level.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereClass' => 
            array (
              'name' => 'hereClass',
              'desc' => 'prop_wayfinder.hereClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'active',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the items showing where you are, all the way up the chain.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentClass' => 
            array (
              'name' => 'parentClass',
              'desc' => 'prop_wayfinder.parentClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for menu items that are a container and have children.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowClass' => 
            array (
              'name' => 'rowClass',
              'desc' => 'prop_wayfinder.rowClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class denoting each output row.',
              'area' => '',
              'area_trans' => '',
            ),
            'outerClass' => 
            array (
              'name' => 'outerClass',
              'desc' => 'prop_wayfinder.outerClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the outer template.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerClass' => 
            array (
              'name' => 'innerClass',
              'desc' => 'prop_wayfinder.innerClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the inner template.',
              'area' => '',
              'area_trans' => '',
            ),
            'levelClass' => 
            array (
              'name' => 'levelClass',
              'desc' => 'prop_wayfinder.levelClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class denoting every output row level. The level number will be added to the specified class (level1, level2, level3 etc if you specified "level").',
              'area' => '',
              'area_trans' => '',
            ),
            'selfClass' => 
            array (
              'name' => 'selfClass',
              'desc' => 'prop_wayfinder.selfClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the current item.',
              'area' => '',
              'area_trans' => '',
            ),
            'webLinkClass' => 
            array (
              'name' => 'webLinkClass',
              'desc' => 'prop_wayfinder.webLinkClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for weblink items.',
              'area' => '',
              'area_trans' => '',
            ),
            'outerTpl' => 
            array (
              'name' => 'outerTpl',
              'desc' => 'prop_wayfinder.outerTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the outer most container; if not included, a string including "<ul>[[+wf.wrapper]]</ul>" is assumed.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowTpl' => 
            array (
              'name' => 'rowTpl',
              'desc' => 'prop_wayfinder.rowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the regular row items.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentRowTpl' => 
            array (
              'name' => 'parentRowTpl',
              'desc' => 'prop_wayfinder.parentRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for any Resource that is a container and has children. Remember the [wf.wrapper] placeholder to output the children documents.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentRowHereTpl' => 
            array (
              'name' => 'parentRowHereTpl',
              'desc' => 'prop_wayfinder.parentRowHereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource if it is a container and has children. Remember the [wf.wrapper] placeholder to output the children documents.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereTpl' => 
            array (
              'name' => 'hereTpl',
              'desc' => 'prop_wayfinder.hereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerTpl' => 
            array (
              'name' => 'innerTpl',
              'desc' => 'prop_wayfinder.innerTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for each submenu. If no innerTpl is specified the outerTpl is used in its place.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerRowTpl' => 
            array (
              'name' => 'innerRowTpl',
              'desc' => 'prop_wayfinder.innerRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the row items in a subfolder.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerHereTpl' => 
            array (
              'name' => 'innerHereTpl',
              'desc' => 'prop_wayfinder.innerHereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource if it is in a subfolder.',
              'area' => '',
              'area_trans' => '',
            ),
            'activeParentRowTpl' => 
            array (
              'name' => 'activeParentRowTpl',
              'desc' => 'prop_wayfinder.activeParentRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for items that are containers, have children and are currently active in the tree.',
              'area' => '',
              'area_trans' => '',
            ),
            'categoryFoldersTpl' => 
            array (
              'name' => 'categoryFoldersTpl',
              'desc' => 'prop_wayfinder.categoryFoldersTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for category folders. Category folders are determined by setting the template to blank or by setting the link attributes field to rel="category".',
              'area' => '',
              'area_trans' => '',
            ),
            'startItemTpl' => 
            array (
              'name' => 'startItemTpl',
              'desc' => 'prop_wayfinder.startItemTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the start item, if enabled via the &displayStart parameter. Note: the default template shows the start item but does not link it. If you do not need a link, a class can be applied to the default template using the parameter &firstClass=`className`.',
              'area' => '',
              'area_trans' => '',
            ),
            'permissions' => 
            array (
              'name' => 'permissions',
              'desc' => 'prop_wayfinder.permissions_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'list',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Will check for a permission on the Resource. Defaults to "list" - set to blank to skip normal permissions checks.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereId' => 
            array (
              'name' => 'hereId',
              'desc' => 'prop_wayfinder.hereId_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. If set, will change the "here" Resource to this ID. Defaults to the currently active Resource.',
              'area' => '',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'prop_wayfinder.where_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. A JSON object for where conditions for all items selected in the menu.',
              'area' => '',
              'area_trans' => '',
            ),
            'templates' => 
            array (
              'name' => 'templates',
              'desc' => 'prop_wayfinder.templates_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. A comma-separated list of Template IDs to restrict selected Resources to.',
              'area' => '',
              'area_trans' => '',
            ),
            'previewUnpublished' => 
            array (
              'name' => 'previewUnpublished',
              'desc' => 'prop_wayfinder.previewunpublished_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. If set to Yes, if you are logged into the mgr and have the view_unpublished permission, it will allow previewing of unpublished resources in your menus in the front-end.',
              'area' => '',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * Wayfinder Snippet to build site navigation menus
 *
 * Totally refactored from original DropMenu nav builder to make it easier to
 * create custom navigation by using chunks as output templates. By using
 * templates, many of the paramaters are no longer needed for flexible output
 * including tables, unordered- or ordered-lists (ULs or OLs), definition lists
 * (DLs) or in any other format you desire.
 *
 * @version 2.1.1-beta5
 * @author Garry Nutting (collabpad.com)
 * @author Kyle Jaebker (muddydogpaws.com)
 * @author Ryan Thrash (modx.com)
 * @author Shaun McCormick (modx.com)
 * @author Jason Coward (modx.com)
 *
 * @example [[Wayfinder? &startId=`0`]]
 *
 * @var modX $modx
 * @var array $scriptProperties
 * 
 * @package wayfinder
 */
$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');

/* include a custom config file if specified */
if (isset($scriptProperties[\'config\'])) {
    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';
} else {
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';
}
if (file_exists($scriptProperties[\'config\'])) {
    include $scriptProperties[\'config\'];
}

/* include wayfinder class */
include_once $wayfinder_base.\'wayfinder.class.php\';
if (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {
    return \'error: Wayfinder class not found\';
}
$wf = new Wayfinder($modx,$scriptProperties);

/* get user class definitions
 * TODO: eventually move these into config parameters */
$wf->_css = array(
    \'first\' => isset($firstClass) ? $firstClass : \'\',
    \'last\' => isset($lastClass) ? $lastClass : \'last\',
    \'here\' => isset($hereClass) ? $hereClass : \'active\',
    \'parent\' => isset($parentClass) ? $parentClass : \'\',
    \'row\' => isset($rowClass) ? $rowClass : \'\',
    \'outer\' => isset($outerClass) ? $outerClass : \'\',
    \'inner\' => isset($innerClass) ? $innerClass : \'\',
    \'level\' => isset($levelClass) ? $levelClass: \'\',
    \'self\' => isset($selfClass) ? $selfClass : \'\',
    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'
);

/* get user templates
 * TODO: eventually move these into config parameters */
$wf->_templates = array(
    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',
    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',
    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',
    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',
    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',
    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',
    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',
    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',
    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',
    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',
    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'
);

/* process Wayfinder */
$output = $wf->run();
if ($wf->_config[\'debug\']) {
    $output .= $wf->renderDebugOutput();
}

/* output results */
if ($wf->_config[\'ph\']) {
    $modx->setPlaceholder($wf->_config[\'ph\'],$output);
} else {
    return $output;
}',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'Breadcrumbs' => 
      array (
        'fields' => 
        array (
          'id' => 45,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Breadcrumbs',
          'description' => '',
          'editor_type' => 0,
          'category' => 14,
          'cache_type' => 0,
          'snippet' => '/**
 * BreadCrumbs
 *
 * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>
 *
 * BreadCrumbs is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package breadcrumbs
 */
/**
 * @name BreadCrumbs
 * @version 0.9f
 * @created 2006-06-12
 * @since 2009-05-11
 * @author Jared <jaredc@honeydewdesign.com>
 * @editor Bill Wilson
 * @editor wendy@djamoer.net
 * @editor grad
 * @editor Shaun McCormick <shaun@collabpad.com>
 * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>
 * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>
 * @tester Bob Ray
 * @package breadcrumbs
 *
 * This snippet was designed to show the path through the various levels of site
 * structure back to the root. It is NOT necessarily the path the user took to
 * arrive at a given page.
 *
 * @see breadcrumbs.class.php for config settings
 *
 * Included classes
 * .B_crumbBox Span that surrounds all crumb output
 * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown
 * .B_currentCrumb Span or A tag surrounding the current crumb
 * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not
 * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .
 * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or
 * hide)
 * .B_homeCrumb Class given to the home crumb
 */
require_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';
$bc = new BreadCrumbs($modx,$scriptProperties);
return $bc->run();',
          'locked' => false,
          'properties' => NULL,
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * BreadCrumbs
 *
 * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>
 *
 * BreadCrumbs is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package breadcrumbs
 */
/**
 * @name BreadCrumbs
 * @version 0.9f
 * @created 2006-06-12
 * @since 2009-05-11
 * @author Jared <jaredc@honeydewdesign.com>
 * @editor Bill Wilson
 * @editor wendy@djamoer.net
 * @editor grad
 * @editor Shaun McCormick <shaun@collabpad.com>
 * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>
 * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>
 * @tester Bob Ray
 * @package breadcrumbs
 *
 * This snippet was designed to show the path through the various levels of site
 * structure back to the root. It is NOT necessarily the path the user took to
 * arrive at a given page.
 *
 * @see breadcrumbs.class.php for config settings
 *
 * Included classes
 * .B_crumbBox Span that surrounds all crumb output
 * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown
 * .B_currentCrumb Span or A tag surrounding the current crumb
 * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not
 * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .
 * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or
 * hide)
 * .B_homeCrumb Class given to the home crumb
 */
require_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';
$bc = new BreadCrumbs($modx,$scriptProperties);
return $bc->run();',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'If' => 
      array (
        'fields' => 
        array (
          'id' => 46,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'If',
          'description' => 'Simple if (conditional) snippet',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * If
 *
 * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick
 * <shaun@modx.com>
 *
 * If is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * If is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package if
 */
/**
 * Simple if (conditional) snippet
 *
 * @package if
 */
if (!empty($debug)) {
    print_r($scriptProperties);
    if (!empty($die)) die();
}
$modx->parser->processElementTags(\'\',$subject,true,true);

$output = \'\';
$operator = !empty($operator) ? $operator : \'\';
$operand = !isset($operand) ? \'\' : $operand;
if (isset($subject)) {
    if (!empty($operator)) {
        $operator = strtolower($operator);
        switch ($operator) {
            case \'!=\':
            case \'neq\':
            case \'not\':
            case \'isnot\':
            case \'isnt\':
            case \'unequal\':
            case \'notequal\':
                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<\':
            case \'lt\':
            case \'less\':
            case \'lessthan\':
                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>\':
            case \'gt\':
            case \'greater\':
            case \'greaterthan\':
                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<=\':
            case \'lte\':
            case \'lessthanequals\':
            case \'lessthanorequalto\':
                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>=\':
            case \'gte\':
            case \'greaterthanequals\':
            case \'greaterthanequalto\':
                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'isempty\':
            case \'empty\':
                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'!empty\':
            case \'notempty\':
            case \'isnotempty\':
                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'isnull\':
            case \'null\':
                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'inarray\':
            case \'in_array\':
            case \'ia\':
                $operand = explode(\',\',$operand);
                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'==\':
            case \'=\':
            case \'eq\':
            case \'is\':
            case \'equal\':
            case \'equals\':
            case \'equalto\':
            default:
                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
        }
    }
}
if (!empty($debug)) { var_dump($output); }
unset($subject);
return $output;',
          'locked' => false,
          'properties' => 
          array (
            'subject' => 
            array (
              'name' => 'subject',
              'desc' => 'The data being affected.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The data being affected.',
              'area_trans' => '',
            ),
            'operator' => 
            array (
              'name' => 'operator',
              'desc' => 'The type of conditional.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'EQ',
                  'text' => 'EQ',
                  'name' => 'EQ',
                ),
                1 => 
                array (
                  'value' => 'NEQ',
                  'text' => 'NEQ',
                  'name' => 'NEQ',
                ),
                2 => 
                array (
                  'value' => 'LT',
                  'text' => 'LT',
                  'name' => 'LT',
                ),
                3 => 
                array (
                  'value' => 'GT',
                  'text' => 'GT',
                  'name' => 'GT',
                ),
                4 => 
                array (
                  'value' => 'LTE',
                  'text' => 'LTE',
                  'name' => 'LTE',
                ),
                5 => 
                array (
                  'value' => 'GT',
                  'text' => 'GTE',
                  'name' => 'GTE',
                ),
                6 => 
                array (
                  'value' => 'EMPTY',
                  'text' => 'EMPTY',
                  'name' => 'EMPTY',
                ),
                7 => 
                array (
                  'value' => 'NOTEMPTY',
                  'text' => 'NOTEMPTY',
                  'name' => 'NOTEMPTY',
                ),
                8 => 
                array (
                  'value' => 'ISNULL',
                  'text' => 'ISNULL',
                  'name' => 'ISNULL',
                ),
                9 => 
                array (
                  'value' => 'inarray',
                  'text' => 'INARRAY',
                  'name' => 'INARRAY',
                ),
              ),
              'value' => 'EQ',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The type of conditional.',
              'area_trans' => '',
            ),
            'operand' => 
            array (
              'name' => 'operand',
              'desc' => 'When comparing to the subject, this is the data to compare to.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'When comparing to the subject, this is the data to compare to.',
              'area_trans' => '',
            ),
            'then' => 
            array (
              'name' => 'then',
              'desc' => 'If conditional was successful, output this.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If conditional was successful, output this.',
              'area_trans' => '',
            ),
            'else' => 
            array (
              'name' => 'else',
              'desc' => 'If conditional was unsuccessful, output this.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If conditional was unsuccessful, output this.',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'Will output the parameters passed in, as well as the end output. Leave off when not testing.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Will output the parameters passed in, as well as the end output. Leave off when not testing.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * If
 *
 * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick
 * <shaun@modx.com>
 *
 * If is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * If is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package if
 */
/**
 * Simple if (conditional) snippet
 *
 * @package if
 */
if (!empty($debug)) {
    print_r($scriptProperties);
    if (!empty($die)) die();
}
$modx->parser->processElementTags(\'\',$subject,true,true);

$output = \'\';
$operator = !empty($operator) ? $operator : \'\';
$operand = !isset($operand) ? \'\' : $operand;
if (isset($subject)) {
    if (!empty($operator)) {
        $operator = strtolower($operator);
        switch ($operator) {
            case \'!=\':
            case \'neq\':
            case \'not\':
            case \'isnot\':
            case \'isnt\':
            case \'unequal\':
            case \'notequal\':
                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<\':
            case \'lt\':
            case \'less\':
            case \'lessthan\':
                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>\':
            case \'gt\':
            case \'greater\':
            case \'greaterthan\':
                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<=\':
            case \'lte\':
            case \'lessthanequals\':
            case \'lessthanorequalto\':
                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>=\':
            case \'gte\':
            case \'greaterthanequals\':
            case \'greaterthanequalto\':
                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'isempty\':
            case \'empty\':
                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'!empty\':
            case \'notempty\':
            case \'isnotempty\':
                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'isnull\':
            case \'null\':
                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'inarray\':
            case \'in_array\':
            case \'ia\':
                $operand = explode(\',\',$operand);
                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'==\':
            case \'=\':
            case \'eq\':
            case \'is\':
            case \'equal\':
            case \'equals\':
            case \'equalto\':
            default:
                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
        }
    }
}
if (!empty($debug)) { var_dump($output); }
unset($subject);
return $output;',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'Heading_title' => 
      array (
        'fields' => 
        array (
          'id' => 14,
          'source' => 0,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'Heading_title',
          'caption' => 'Banner Text',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'Left_content' => 
      array (
        'fields' => 
        array (
          'id' => 12,
          'source' => 0,
          'property_preprocess' => false,
          'type' => 'richtext',
          'name' => 'Left_content',
          'caption' => 'Page Left Column Content',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'Right_content' => 
      array (
        'fields' => 
        array (
          'id' => 13,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'richtext',
          'name' => 'Right_content',
          'caption' => 'Page Right Column Content',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);