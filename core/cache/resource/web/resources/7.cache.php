<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 7,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Contact us',
    'longtitle' => '',
    'description' => '',
    'alias' => 'contact-us',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 9,
    'menuindex' => 6,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 2,
    'createdon' => 1410198530,
    'editedby' => 1,
    'editedon' => 1413373921,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1410198360,
    'publishedby' => 2,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => '',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'Left_content' => 
    array (
      0 => 'Left_content',
      1 => '<h2>Get in touch</h2>
<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.</p>
<p>[[$contactForm]]</p>',
      2 => 'default',
      3 => NULL,
      4 => 'richtext',
    ),
    'Right_content' => 
    array (
      0 => 'Right_content',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'richtext',
    ),
    'Heading_title' => 
    array (
      0 => 'Heading_title',
      1 => 'Getting better at what you do, everyday',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NGA</title>

    <!-- Bootstrap -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="about">
    	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         <ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last active"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
         </div> </div>  
    <header id="main-header">
		<img id="header-banner" src="assets/images/bannerabout.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="index.php?id=8" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="http://nga.loyaltymatters.co.uk/"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             <ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last active"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">Getting better at what you do, everyday</h1>
            </div>            
            <!-- Page Headline Start-->
        </div>
    </header>
    <!-- HEADER END->
	
    <!-- Breadcumb START-->
    <div class="block_breadcumb"> 
     <div class="container">
    	<div class="col-lg-12">
        	<ul class="B_crumbBox"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>
 > <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Contact us" href="index.php?id=7"><span itemprop="title">Contact us</span></a></li>
</ul>
        </div>
     </div>
    </div>    
    <!-- Breadcumb End-->

    <!-- General Page 2 Column content Start-->

    <div class="block_twocolumn"> 
    <div class="container">
        <div class="col-lg-7 col-md-7 col-sm-7">
            <h2>Get in touch</h2>
<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.</p>
<p>[[!FormIt? &hooks=`spam,email` 
&fsFormTopic=`Contact Form`  
&emailTpl=`contactFormMessage` 
&emailSubject=`New Website Enquiry ([[+subject]])`
&emailTo=`haque.mdmanzurul@gmail.com,status@loyaltymatters.co.uk` 
&clearFieldsOnSuccess=`1`
&emailHtml=`0`
&validate=`name:required,email:email:required,message:required,subject:required`]]


<form id="contactform" action="index.php?id=7" method="post" class="validateform" name="send-contact">
    <div id="sendmessage">
        Your message has been sent. Thanks for contacting with us.
    </div>
    <div class="row">
        <div class="col-md-12 field">
            <input type="text" name="name" placeholder="Name(required)" data-rule="maxlen:4" data-msg="Please enter at least 4 chars"  value="[[!+fi.name]]" />
            <div class="validation">
            </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6 field">
            <input type="text" name="email" placeholder="Email(required)" data-rule="email" data-msg="Please enter a valid email"  value="[[!+fi.email]]" />
            <div class="validation">
            </div>
        </div>
        <div class="col-md-6 field">
            <input type="text" name="phone" placeholder="Phone(required)" data-rule="maxlen:4" data-msg="Please enter a valid phone number" value="[[!+fi.phone]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 field">
            <input type="text" name="subject" placeholder="Subject(required)" data-rule="maxlen:4" data-msg="Please enter a subject" value="[[!+fi.subject]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 margintop10 field">
            <textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"  value="[[!+fi.message]]"></textarea>
    <div class="validation">
    </div>
    <p>
        <button class="btn btn-theme margintop10 pull-left" type="submit">Send message</button>
         <span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
    </p>
</div>
</div>
</form></p>

        </div>
        <div class="col-lg-5 col-md-5 col-sm-5">
					[[!StoreLocator?
  &searchZoom=`10`
  &zoom=`8`
  &width=`100%`
  &height=`300`
  &centerLongitude=`-1.536593`
  &centerLatitude=`53.989623`
  &storeRowTpl=`MapRowTpl`&storeInfoWindowTpl=`MapMarkerInfoWindow`
]]
[[+StoreLocator.map]]  
[[+StoreLocator.storeList]]
<div class="col-lg-7 company_address"><strong>Nick Girling Associates</strong><br/>104 Station Parade<br/>Harrogate<br/>HG1 1HQ</div>
<div class="col-lg-5 company_address">+44(0)7900564879<br/>info@nickgirlingassociates.com</div>
           
        </div>        
    </div>
    </div>

    <!-- General Page 2 Column content End-->
    
	 <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->

	
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		<ul class="footer_nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last active"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->
    <!-- jQuery (necessary for Bootstrap\'s JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Parallax with stellar.js-->
    <script src="/assets/js/parallax.js"></script>
	<script>
    $(function(){
        $.stellar({
            horizontalScrolling: false,
            verticalOffset: -150
        });
    });
    </script>    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="/assets/js/validate.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
  </body>
</html>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]' => '<ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last active"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>',
    '[[~8]]' => 'index.php?id=8',
    '[[*Heading_title]]' => 'Getting better at what you do, everyday',
    '[[$Header]]' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         <ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last active"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
         </div> </div>  
    <header id="main-header">
		<img id="header-banner" src="assets/images/bannerabout.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="index.php?id=8" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="http://nga.loyaltymatters.co.uk/"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             <ul class="nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last active"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">Getting better at what you do, everyday</h1>
            </div>            
            <!-- Page Headline Start-->
        </div>
    </header>
    <!-- HEADER END->',
    '[[~7]]' => 'index.php?id=7',
    '[[$?resource=`7`&description=`Contact us`&text=`Contact us`]]' => '<a class="B_currentCrumb" itemprop="url" rel="Contact us" href="index.php?id=7"><span itemprop="title">Contact us</span></a>',
    '[[~1]]' => 'index.php?id=1',
    '[[$?description=`Home`&text=`Home`]]' => '<a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a>',
    '[[$?text=`<a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a>`]]' => '<li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>',
    '[[$?text=`<a class="B_currentCrumb" itemprop="url" rel="Contact us" href="index.php?id=7"><span itemprop="title">Contact us</span></a>`]]' => '<li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Contact us" href="index.php?id=7"><span itemprop="title">Contact us</span></a></li>',
    '[[$?text=`<li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>
 > <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Contact us" href="index.php?id=7"><span itemprop="title">Contact us</span></a></li>
`]]' => '<ul class="B_crumbBox"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>
 > <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Contact us" href="index.php?id=7"><span itemprop="title">Contact us</span></a></li>
</ul>',
    '[[Breadcrumbs? &crumbSeparator=`>`]]' => '<ul class="B_crumbBox"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Home" href="index.php?id=1"><span itemprop="title">Home</span></a></li>
 > <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Contact us" href="index.php?id=7"><span itemprop="title">Contact us</span></a></li>
</ul>',
    '[[*id]]' => 7,
    '[[$contactForm]]' => '[[!FormIt? &hooks=`spam,email` 
&fsFormTopic=`Contact Form`  
&emailTpl=`contactFormMessage` 
&emailSubject=`New Website Enquiry ([[+subject]])`
&emailTo=`haque.mdmanzurul@gmail.com,status@loyaltymatters.co.uk` 
&clearFieldsOnSuccess=`1`
&emailHtml=`0`
&validate=`name:required,email:email:required,message:required,subject:required`]]


<form id="contactform" action="index.php?id=7" method="post" class="validateform" name="send-contact">
    <div id="sendmessage">
        Your message has been sent. Thanks for contacting with us.
    </div>
    <div class="row">
        <div class="col-md-12 field">
            <input type="text" name="name" placeholder="Name(required)" data-rule="maxlen:4" data-msg="Please enter at least 4 chars"  value="[[!+fi.name]]" />
            <div class="validation">
            </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6 field">
            <input type="text" name="email" placeholder="Email(required)" data-rule="email" data-msg="Please enter a valid email"  value="[[!+fi.email]]" />
            <div class="validation">
            </div>
        </div>
        <div class="col-md-6 field">
            <input type="text" name="phone" placeholder="Phone(required)" data-rule="maxlen:4" data-msg="Please enter a valid phone number" value="[[!+fi.phone]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 field">
            <input type="text" name="subject" placeholder="Subject(required)" data-rule="maxlen:4" data-msg="Please enter a subject" value="[[!+fi.subject]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 margintop10 field">
            <textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"  value="[[!+fi.message]]"></textarea>
    <div class="validation">
    </div>
    <p>
        <button class="btn btn-theme margintop10 pull-left" type="submit">Send message</button>
         <span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
    </p>
</div>
</div>
</form>',
    '[[*Left_content]]' => '<h2>Get in touch</h2>
<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.</p>
<p>[[!FormIt? &hooks=`spam,email` 
&fsFormTopic=`Contact Form`  
&emailTpl=`contactFormMessage` 
&emailSubject=`New Website Enquiry ([[+subject]])`
&emailTo=`haque.mdmanzurul@gmail.com,status@loyaltymatters.co.uk` 
&clearFieldsOnSuccess=`1`
&emailHtml=`0`
&validate=`name:required,email:email:required,message:required,subject:required`]]


<form id="contactform" action="index.php?id=7" method="post" class="validateform" name="send-contact">
    <div id="sendmessage">
        Your message has been sent. Thanks for contacting with us.
    </div>
    <div class="row">
        <div class="col-md-12 field">
            <input type="text" name="name" placeholder="Name(required)" data-rule="maxlen:4" data-msg="Please enter at least 4 chars"  value="[[!+fi.name]]" />
            <div class="validation">
            </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6 field">
            <input type="text" name="email" placeholder="Email(required)" data-rule="email" data-msg="Please enter a valid email"  value="[[!+fi.email]]" />
            <div class="validation">
            </div>
        </div>
        <div class="col-md-6 field">
            <input type="text" name="phone" placeholder="Phone(required)" data-rule="maxlen:4" data-msg="Please enter a valid phone number" value="[[!+fi.phone]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 field">
            <input type="text" name="subject" placeholder="Subject(required)" data-rule="maxlen:4" data-msg="Please enter a subject" value="[[!+fi.subject]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 margintop10 field">
            <textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"  value="[[!+fi.message]]"></textarea>
    <div class="validation">
    </div>
    <p>
        <button class="btn btn-theme margintop10 pull-left" type="submit">Send message</button>
         <span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
    </p>
</div>
</div>
</form></p>',
    '[[*Right_content]]' => '',
    '[[$Services]]' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
    '[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]' => '<ul class="footer_nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last active"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>',
    '[[$Footer]]' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		<ul class="footer_nav"><li class="first"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last active"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
    '[[%storelocator.address_not_found]]' => 'Address not found, please try again',
    '[[getResourceField? &id=`7` &field=`MapImage` &processTV=`1`]]' => '',
    '[[getResourceField? &id=`7` &field=`MapPostcode` &processTV=`1`]]' => '',
    '[[%storelocator.address]]' => 'Enter your address',
    '[[%storelocator.radius]]' => 'Radius',
    '[[%storelocator.submit]]' => 'Search',
    '[[%storelocator.clear]]' => 'Clear',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'Header' => 
      array (
        'fields' => 
        array (
          'id' => 8,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Header',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
         </div> </div>  
    <header id="main-header">
		<img id="header-banner" src="assets/images/bannerabout.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="[[~8]]" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="[[++site_url]]"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">[[*Heading_title]]</h1>
            </div>            
            <!-- Page Headline Start-->
        </div>
    </header>
    <!-- HEADER END->',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
         </div> </div>  
    <header id="main-header">
		<img id="header-banner" src="assets/images/bannerabout.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="[[~8]]" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="[[++site_url]]"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">[[*Heading_title]]</h1>
            </div>            
            <!-- Page Headline Start-->
        </div>
    </header>
    <!-- HEADER END->',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'contactForm' => 
      array (
        'fields' => 
        array (
          'id' => 11,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'ContactForm',
          'description' => '',
          'editor_type' => 0,
          'category' => 15,
          'cache_type' => 0,
          'snippet' => '[[!FormIt? &hooks=`spam,email` 
&fsFormTopic=`Contact Form`  
&emailTpl=`contactFormMessage` 
&emailSubject=`New Website Enquiry ([[+subject]])`
&emailTo=`haque.mdmanzurul@gmail.com,status@loyaltymatters.co.uk` 
&clearFieldsOnSuccess=`1`
&emailHtml=`0`
&validate=`name:required,email:email:required,message:required,subject:required`]]


<form id="contactform" action="[[~[[*id]]]]" method="post" class="validateform" name="send-contact">
    <div id="sendmessage">
        Your message has been sent. Thanks for contacting with us.
    </div>
    <div class="row">
        <div class="col-md-12 field">
            <input type="text" name="name" placeholder="Name(required)" data-rule="maxlen:4" data-msg="Please enter at least 4 chars"  value="[[!+fi.name]]" />
            <div class="validation">
            </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6 field">
            <input type="text" name="email" placeholder="Email(required)" data-rule="email" data-msg="Please enter a valid email"  value="[[!+fi.email]]" />
            <div class="validation">
            </div>
        </div>
        <div class="col-md-6 field">
            <input type="text" name="phone" placeholder="Phone(required)" data-rule="maxlen:4" data-msg="Please enter a valid phone number" value="[[!+fi.phone]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 field">
            <input type="text" name="subject" placeholder="Subject(required)" data-rule="maxlen:4" data-msg="Please enter a subject" value="[[!+fi.subject]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 margintop10 field">
            <textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"  value="[[!+fi.message]]"></textarea>
    <div class="validation">
    </div>
    <p>
        <button class="btn btn-theme margintop10 pull-left" type="submit">Send message</button>
         <span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
    </p>
</div>
</div>
</form>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[!FormIt? &hooks=`spam,email` 
&fsFormTopic=`Contact Form`  
&emailTpl=`contactFormMessage` 
&emailSubject=`New Website Enquiry ([[+subject]])`
&emailTo=`haque.mdmanzurul@gmail.com,status@loyaltymatters.co.uk` 
&clearFieldsOnSuccess=`1`
&emailHtml=`0`
&validate=`name:required,email:email:required,message:required,subject:required`]]


<form id="contactform" action="[[~[[*id]]]]" method="post" class="validateform" name="send-contact">
    <div id="sendmessage">
        Your message has been sent. Thanks for contacting with us.
    </div>
    <div class="row">
        <div class="col-md-12 field">
            <input type="text" name="name" placeholder="Name(required)" data-rule="maxlen:4" data-msg="Please enter at least 4 chars"  value="[[!+fi.name]]" />
            <div class="validation">
            </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6 field">
            <input type="text" name="email" placeholder="Email(required)" data-rule="email" data-msg="Please enter a valid email"  value="[[!+fi.email]]" />
            <div class="validation">
            </div>
        </div>
        <div class="col-md-6 field">
            <input type="text" name="phone" placeholder="Phone(required)" data-rule="maxlen:4" data-msg="Please enter a valid phone number" value="[[!+fi.phone]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 field">
            <input type="text" name="subject" placeholder="Subject(required)" data-rule="maxlen:4" data-msg="Please enter a subject" value="[[!+fi.subject]]" />
            <div class="validation">
            </div>
        </div>        
    </div>
    <div class="row">    
        <div class="col-md-12 margintop10 field">
            <textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"  value="[[!+fi.message]]"></textarea>
    <div class="validation">
    </div>
    <p>
        <button class="btn btn-theme margintop10 pull-left" type="submit">Send message</button>
         <span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
    </p>
</div>
</div>
</form>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'Services' => 
      array (
        'fields' => 
        array (
          'id' => 9,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Services',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'Footer' => 
      array (
        'fields' => 
        array (
          'id' => 7,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'Wayfinder' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Wayfinder',
          'description' => 'Wayfinder for MODx Revolution 2.0.0-beta-5 and later.',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * Wayfinder Snippet to build site navigation menus
 *
 * Totally refactored from original DropMenu nav builder to make it easier to
 * create custom navigation by using chunks as output templates. By using
 * templates, many of the paramaters are no longer needed for flexible output
 * including tables, unordered- or ordered-lists (ULs or OLs), definition lists
 * (DLs) or in any other format you desire.
 *
 * @version 2.1.1-beta5
 * @author Garry Nutting (collabpad.com)
 * @author Kyle Jaebker (muddydogpaws.com)
 * @author Ryan Thrash (modx.com)
 * @author Shaun McCormick (modx.com)
 * @author Jason Coward (modx.com)
 *
 * @example [[Wayfinder? &startId=`0`]]
 *
 * @var modX $modx
 * @var array $scriptProperties
 * 
 * @package wayfinder
 */
$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');

/* include a custom config file if specified */
if (isset($scriptProperties[\'config\'])) {
    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';
} else {
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';
}
if (file_exists($scriptProperties[\'config\'])) {
    include $scriptProperties[\'config\'];
}

/* include wayfinder class */
include_once $wayfinder_base.\'wayfinder.class.php\';
if (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {
    return \'error: Wayfinder class not found\';
}
$wf = new Wayfinder($modx,$scriptProperties);

/* get user class definitions
 * TODO: eventually move these into config parameters */
$wf->_css = array(
    \'first\' => isset($firstClass) ? $firstClass : \'\',
    \'last\' => isset($lastClass) ? $lastClass : \'last\',
    \'here\' => isset($hereClass) ? $hereClass : \'active\',
    \'parent\' => isset($parentClass) ? $parentClass : \'\',
    \'row\' => isset($rowClass) ? $rowClass : \'\',
    \'outer\' => isset($outerClass) ? $outerClass : \'\',
    \'inner\' => isset($innerClass) ? $innerClass : \'\',
    \'level\' => isset($levelClass) ? $levelClass: \'\',
    \'self\' => isset($selfClass) ? $selfClass : \'\',
    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'
);

/* get user templates
 * TODO: eventually move these into config parameters */
$wf->_templates = array(
    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',
    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',
    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',
    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',
    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',
    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',
    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',
    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',
    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',
    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',
    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'
);

/* process Wayfinder */
$output = $wf->run();
if ($wf->_config[\'debug\']) {
    $output .= $wf->renderDebugOutput();
}

/* output results */
if ($wf->_config[\'ph\']) {
    $modx->setPlaceholder($wf->_config[\'ph\'],$output);
} else {
    return $output;
}',
          'locked' => false,
          'properties' => 
          array (
            'level' => 
            array (
              'name' => 'level',
              'desc' => 'prop_wayfinder.level_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Depth (number of levels) to build the menu from. 0 goes through all levels.',
              'area' => '',
              'area_trans' => '',
            ),
            'includeDocs' => 
            array (
              'name' => 'includeDocs',
              'desc' => 'prop_wayfinder.includeDocs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Acts as a filter and will limit the output to only the documents specified in this parameter. The startId is still required.',
              'area' => '',
              'area_trans' => '',
            ),
            'excludeDocs' => 
            array (
              'name' => 'excludeDocs',
              'desc' => 'prop_wayfinder.excludeDocs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Acts as a filter and will remove the documents specified in this parameter from the output. The startId is still required.',
              'area' => '',
              'area_trans' => '',
            ),
            'contexts' => 
            array (
              'name' => 'contexts',
              'desc' => 'prop_wayfinder.contexts_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Specify the contexts for the Resources that will be loaded in this menu. Useful when used with startId at 0 to show all first-level items. Note: This will increase load times a bit, but if you set cacheResults to 1, that will offset the load time.',
              'area' => '',
              'area_trans' => '',
            ),
            'cacheResults' => 
            array (
              'name' => 'cacheResults',
              'desc' => 'prop_wayfinder.cacheResults_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Cache the generated menu to the MODX Resource cache. Setting this to 1 will speed up the loading of your menus.',
              'area' => '',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'prop_wayfinder.cacheTime_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 3600,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The number of seconds to store the cached menu, if cacheResults is 1. Set to 0 to store indefinitely until cache is manually cleared.',
              'area' => '',
              'area_trans' => '',
            ),
            'ph' => 
            array (
              'name' => 'ph',
              'desc' => 'prop_wayfinder.ph_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'To display send the output of Wayfinder to a placeholder set the ph parameter equal to the name of the desired placeholder. All output including the debugging (if on) will be sent to the placeholder specified.',
              'area' => '',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'prop_wayfinder.debug_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'With the debug parameter set to 1, Wayfinder will output information on how each Resource was processed.',
              'area' => '',
              'area_trans' => '',
            ),
            'ignoreHidden' => 
            array (
              'name' => 'ignoreHidden',
              'desc' => 'prop_wayfinder.ignoreHidden_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The ignoreHidden parameter allows Wayfinder to ignore the display in menu flag that can be set for each document. With this parameter set to 1, all Resources will be displayed regardless of the Display in Menu flag.',
              'area' => '',
              'area_trans' => '',
            ),
            'hideSubMenus' => 
            array (
              'name' => 'hideSubMenus',
              'desc' => 'prop_wayfinder.hideSubMenus_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The hideSubMenus parameter will remove all non-active submenus from the Wayfinder output if set to 1. This parameter only works if multiple levels are being displayed.',
              'area' => '',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'prop_wayfinder.useWeblinkUrl_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => ' If WebLinks are used in the output, Wayfinder will output the link specified in the WebLink instead of the normal MODx link. To use the standard display of WebLinks (like any other Resource) set this to 0.',
              'area' => '',
              'area_trans' => '',
            ),
            'fullLink' => 
            array (
              'name' => 'fullLink',
              'desc' => 'prop_wayfinder.fullLink_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'If set to 1, will display the entire, absolute URL in the link. (It is recommended to use scheme instead.)',
              'area' => '',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'prop_wayfinder.scheme_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'prop_wayfinder.relative',
                  'value' => '',
                  'name' => 'Relative',
                ),
                1 => 
                array (
                  'text' => 'prop_wayfinder.absolute',
                  'value' => 'abs',
                  'name' => 'Absolute',
                ),
                2 => 
                array (
                  'text' => 'prop_wayfinder.full',
                  'value' => 'full',
                  'name' => 'Full',
                ),
              ),
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Determines how URLs are generated for each link. Set to "abs" to show the absolute URL, "full" to show the full URL, and blank to use the relative URL. Defaults to relative.',
              'area' => '',
              'area_trans' => '',
            ),
            'sortOrder' => 
            array (
              'name' => 'sortOrder',
              'desc' => 'prop_wayfinder.sortOrder_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'prop_wayfinder.ascending',
                  'value' => 'ASC',
                  'name' => 'Ascending',
                ),
                1 => 
                array (
                  'text' => 'prop_wayfinder.descending',
                  'value' => 'DESC',
                  'name' => 'Descending',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Allows the menu to be sorted in either ascending or descending order.',
              'area' => '',
              'area_trans' => '',
            ),
            'sortBy' => 
            array (
              'name' => 'sortBy',
              'desc' => 'prop_wayfinder.sortBy_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'menuindex',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Sorts the output by any of the Resource fields on a level by level basis. This means that each submenu will be sorted independently of all other submenus at the same level. Random will sort the output differently every time the page is loaded if the snippet is called uncached.',
              'area' => '',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'prop_wayfinder.limit_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Causes Wayfinder to only process the number of items specified per level.',
              'area' => '',
              'area_trans' => '',
            ),
            'cssTpl' => 
            array (
              'name' => 'cssTpl',
              'desc' => 'prop_wayfinder.cssTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This parameter allows for a chunk containing a link to a style sheet or style information to be inserted into the head section of the generated page.',
              'area' => '',
              'area_trans' => '',
            ),
            'jsTpl' => 
            array (
              'name' => 'jsTpl',
              'desc' => 'prop_wayfinder.jsTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This parameter allows for a chunk containing some Javascript to be inserted into the head section of the generated page.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowIdPrefix' => 
            array (
              'name' => 'rowIdPrefix',
              'desc' => 'prop_wayfinder.rowIdPrefix_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'If set, Wayfinder will replace the id placeholder with a unique id consisting of the specified prefix plus the Resource id.',
              'area' => '',
              'area_trans' => '',
            ),
            'textOfLinks' => 
            array (
              'name' => 'textOfLinks',
              'desc' => 'prop_wayfinder.textOfLinks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'menutitle',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This field will be inserted into the linktext placeholder.',
              'area' => '',
              'area_trans' => '',
            ),
            'titleOfLinks' => 
            array (
              'name' => 'titleOfLinks',
              'desc' => 'prop_wayfinder.titleOfLinks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'pagetitle',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This field will be inserted into the linktitle placeholder.',
              'area' => '',
              'area_trans' => '',
            ),
            'displayStart' => 
            array (
              'name' => 'displayStart',
              'desc' => 'prop_wayfinder.displayStart_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Show the document as referenced by startId in the menu.',
              'area' => '',
              'area_trans' => '',
            ),
            'firstClass' => 
            array (
              'name' => 'firstClass',
              'desc' => 'prop_wayfinder.firstClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'first',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the first item at a given menu level.',
              'area' => '',
              'area_trans' => '',
            ),
            'lastClass' => 
            array (
              'name' => 'lastClass',
              'desc' => 'prop_wayfinder.lastClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'last',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the last item at a given menu level.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereClass' => 
            array (
              'name' => 'hereClass',
              'desc' => 'prop_wayfinder.hereClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'active',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the items showing where you are, all the way up the chain.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentClass' => 
            array (
              'name' => 'parentClass',
              'desc' => 'prop_wayfinder.parentClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for menu items that are a container and have children.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowClass' => 
            array (
              'name' => 'rowClass',
              'desc' => 'prop_wayfinder.rowClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class denoting each output row.',
              'area' => '',
              'area_trans' => '',
            ),
            'outerClass' => 
            array (
              'name' => 'outerClass',
              'desc' => 'prop_wayfinder.outerClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the outer template.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerClass' => 
            array (
              'name' => 'innerClass',
              'desc' => 'prop_wayfinder.innerClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the inner template.',
              'area' => '',
              'area_trans' => '',
            ),
            'levelClass' => 
            array (
              'name' => 'levelClass',
              'desc' => 'prop_wayfinder.levelClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class denoting every output row level. The level number will be added to the specified class (level1, level2, level3 etc if you specified "level").',
              'area' => '',
              'area_trans' => '',
            ),
            'selfClass' => 
            array (
              'name' => 'selfClass',
              'desc' => 'prop_wayfinder.selfClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the current item.',
              'area' => '',
              'area_trans' => '',
            ),
            'webLinkClass' => 
            array (
              'name' => 'webLinkClass',
              'desc' => 'prop_wayfinder.webLinkClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for weblink items.',
              'area' => '',
              'area_trans' => '',
            ),
            'outerTpl' => 
            array (
              'name' => 'outerTpl',
              'desc' => 'prop_wayfinder.outerTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the outer most container; if not included, a string including "<ul>[[+wf.wrapper]]</ul>" is assumed.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowTpl' => 
            array (
              'name' => 'rowTpl',
              'desc' => 'prop_wayfinder.rowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the regular row items.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentRowTpl' => 
            array (
              'name' => 'parentRowTpl',
              'desc' => 'prop_wayfinder.parentRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for any Resource that is a container and has children. Remember the [wf.wrapper] placeholder to output the children documents.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentRowHereTpl' => 
            array (
              'name' => 'parentRowHereTpl',
              'desc' => 'prop_wayfinder.parentRowHereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource if it is a container and has children. Remember the [wf.wrapper] placeholder to output the children documents.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereTpl' => 
            array (
              'name' => 'hereTpl',
              'desc' => 'prop_wayfinder.hereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerTpl' => 
            array (
              'name' => 'innerTpl',
              'desc' => 'prop_wayfinder.innerTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for each submenu. If no innerTpl is specified the outerTpl is used in its place.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerRowTpl' => 
            array (
              'name' => 'innerRowTpl',
              'desc' => 'prop_wayfinder.innerRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the row items in a subfolder.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerHereTpl' => 
            array (
              'name' => 'innerHereTpl',
              'desc' => 'prop_wayfinder.innerHereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource if it is in a subfolder.',
              'area' => '',
              'area_trans' => '',
            ),
            'activeParentRowTpl' => 
            array (
              'name' => 'activeParentRowTpl',
              'desc' => 'prop_wayfinder.activeParentRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for items that are containers, have children and are currently active in the tree.',
              'area' => '',
              'area_trans' => '',
            ),
            'categoryFoldersTpl' => 
            array (
              'name' => 'categoryFoldersTpl',
              'desc' => 'prop_wayfinder.categoryFoldersTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for category folders. Category folders are determined by setting the template to blank or by setting the link attributes field to rel="category".',
              'area' => '',
              'area_trans' => '',
            ),
            'startItemTpl' => 
            array (
              'name' => 'startItemTpl',
              'desc' => 'prop_wayfinder.startItemTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the start item, if enabled via the &displayStart parameter. Note: the default template shows the start item but does not link it. If you do not need a link, a class can be applied to the default template using the parameter &firstClass=`className`.',
              'area' => '',
              'area_trans' => '',
            ),
            'permissions' => 
            array (
              'name' => 'permissions',
              'desc' => 'prop_wayfinder.permissions_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'list',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Will check for a permission on the Resource. Defaults to "list" - set to blank to skip normal permissions checks.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereId' => 
            array (
              'name' => 'hereId',
              'desc' => 'prop_wayfinder.hereId_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. If set, will change the "here" Resource to this ID. Defaults to the currently active Resource.',
              'area' => '',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'prop_wayfinder.where_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. A JSON object for where conditions for all items selected in the menu.',
              'area' => '',
              'area_trans' => '',
            ),
            'templates' => 
            array (
              'name' => 'templates',
              'desc' => 'prop_wayfinder.templates_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. A comma-separated list of Template IDs to restrict selected Resources to.',
              'area' => '',
              'area_trans' => '',
            ),
            'previewUnpublished' => 
            array (
              'name' => 'previewUnpublished',
              'desc' => 'prop_wayfinder.previewunpublished_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. If set to Yes, if you are logged into the mgr and have the view_unpublished permission, it will allow previewing of unpublished resources in your menus in the front-end.',
              'area' => '',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * Wayfinder Snippet to build site navigation menus
 *
 * Totally refactored from original DropMenu nav builder to make it easier to
 * create custom navigation by using chunks as output templates. By using
 * templates, many of the paramaters are no longer needed for flexible output
 * including tables, unordered- or ordered-lists (ULs or OLs), definition lists
 * (DLs) or in any other format you desire.
 *
 * @version 2.1.1-beta5
 * @author Garry Nutting (collabpad.com)
 * @author Kyle Jaebker (muddydogpaws.com)
 * @author Ryan Thrash (modx.com)
 * @author Shaun McCormick (modx.com)
 * @author Jason Coward (modx.com)
 *
 * @example [[Wayfinder? &startId=`0`]]
 *
 * @var modX $modx
 * @var array $scriptProperties
 * 
 * @package wayfinder
 */
$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');

/* include a custom config file if specified */
if (isset($scriptProperties[\'config\'])) {
    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';
} else {
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';
}
if (file_exists($scriptProperties[\'config\'])) {
    include $scriptProperties[\'config\'];
}

/* include wayfinder class */
include_once $wayfinder_base.\'wayfinder.class.php\';
if (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {
    return \'error: Wayfinder class not found\';
}
$wf = new Wayfinder($modx,$scriptProperties);

/* get user class definitions
 * TODO: eventually move these into config parameters */
$wf->_css = array(
    \'first\' => isset($firstClass) ? $firstClass : \'\',
    \'last\' => isset($lastClass) ? $lastClass : \'last\',
    \'here\' => isset($hereClass) ? $hereClass : \'active\',
    \'parent\' => isset($parentClass) ? $parentClass : \'\',
    \'row\' => isset($rowClass) ? $rowClass : \'\',
    \'outer\' => isset($outerClass) ? $outerClass : \'\',
    \'inner\' => isset($innerClass) ? $innerClass : \'\',
    \'level\' => isset($levelClass) ? $levelClass: \'\',
    \'self\' => isset($selfClass) ? $selfClass : \'\',
    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'
);

/* get user templates
 * TODO: eventually move these into config parameters */
$wf->_templates = array(
    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',
    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',
    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',
    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',
    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',
    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',
    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',
    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',
    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',
    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',
    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'
);

/* process Wayfinder */
$output = $wf->run();
if ($wf->_config[\'debug\']) {
    $output .= $wf->renderDebugOutput();
}

/* output results */
if ($wf->_config[\'ph\']) {
    $modx->setPlaceholder($wf->_config[\'ph\'],$output);
} else {
    return $output;
}',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'Breadcrumbs' => 
      array (
        'fields' => 
        array (
          'id' => 45,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Breadcrumbs',
          'description' => '',
          'editor_type' => 0,
          'category' => 14,
          'cache_type' => 0,
          'snippet' => '/**
 * BreadCrumbs
 *
 * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>
 *
 * BreadCrumbs is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package breadcrumbs
 */
/**
 * @name BreadCrumbs
 * @version 0.9f
 * @created 2006-06-12
 * @since 2009-05-11
 * @author Jared <jaredc@honeydewdesign.com>
 * @editor Bill Wilson
 * @editor wendy@djamoer.net
 * @editor grad
 * @editor Shaun McCormick <shaun@collabpad.com>
 * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>
 * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>
 * @tester Bob Ray
 * @package breadcrumbs
 *
 * This snippet was designed to show the path through the various levels of site
 * structure back to the root. It is NOT necessarily the path the user took to
 * arrive at a given page.
 *
 * @see breadcrumbs.class.php for config settings
 *
 * Included classes
 * .B_crumbBox Span that surrounds all crumb output
 * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown
 * .B_currentCrumb Span or A tag surrounding the current crumb
 * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not
 * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .
 * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or
 * hide)
 * .B_homeCrumb Class given to the home crumb
 */
require_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';
$bc = new BreadCrumbs($modx,$scriptProperties);
return $bc->run();',
          'locked' => false,
          'properties' => NULL,
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * BreadCrumbs
 *
 * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>
 *
 * BreadCrumbs is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package breadcrumbs
 */
/**
 * @name BreadCrumbs
 * @version 0.9f
 * @created 2006-06-12
 * @since 2009-05-11
 * @author Jared <jaredc@honeydewdesign.com>
 * @editor Bill Wilson
 * @editor wendy@djamoer.net
 * @editor grad
 * @editor Shaun McCormick <shaun@collabpad.com>
 * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>
 * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>
 * @tester Bob Ray
 * @package breadcrumbs
 *
 * This snippet was designed to show the path through the various levels of site
 * structure back to the root. It is NOT necessarily the path the user took to
 * arrive at a given page.
 *
 * @see breadcrumbs.class.php for config settings
 *
 * Included classes
 * .B_crumbBox Span that surrounds all crumb output
 * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown
 * .B_currentCrumb Span or A tag surrounding the current crumb
 * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not
 * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .
 * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or
 * hide)
 * .B_homeCrumb Class given to the home crumb
 */
require_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';
$bc = new BreadCrumbs($modx,$scriptProperties);
return $bc->run();',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'FormIt' => 
      array (
        'fields' => 
        array (
          'id' => 32,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'FormIt',
          'description' => 'A dynamic form processing snippet.',
          'editor_type' => 0,
          'category' => 6,
          'cache_type' => 0,
          'snippet' => '/**
 * FormIt
 *
 * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>
 *
 * FormIt is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package formit
 */
/**
 * FormIt
 *
 * A dynamic form processing Snippet for MODx Revolution.
 *
 * @package formit
 */
require_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\',null,MODX_CORE_PATH).\'components/formit/\').\'model/formit/formit.class.php\';
$fi = new FormIt($modx,$scriptProperties);
$fi->initialize($modx->context->get(\'key\'));
$fi->loadRequest();

$fields = $fi->request->prepare();
return $fi->request->handle($fields);',
          'locked' => false,
          'properties' => 
          array (
            'hooks' => 
            array (
              'name' => 'hooks',
              'desc' => 'prop_formit.hooks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'What scripts to fire, if any, after the form passes validation. This can be a comma-separated list of hooks, and if the first fails, the proceeding ones will not fire. A hook can also be a Snippet name that will execute that Snippet.',
              'area_trans' => '',
            ),
            'preHooks' => 
            array (
              'name' => 'preHooks',
              'desc' => 'prop_formit.prehooks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'What scripts to fire, if any, once the form loads. You can pre-set form fields via $scriptProperties[`hook`]->fields[`fieldname`]. This can be a comma-separated list of hooks, and if the first fails, the proceeding ones will not fire. A hook can also be a Snippet name that will execute that Snippet.',
              'area_trans' => '',
            ),
            'submitVar' => 
            array (
              'name' => 'submitVar',
              'desc' => 'prop_formit.submitvar_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If set, will not begin form processing if this POST variable is not passed.',
              'area_trans' => '',
            ),
            'validate' => 
            array (
              'name' => 'validate',
              'desc' => 'prop_formit.validate_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A comma-separated list of fields to validate, with each field name as name:validator (eg: username:required,email:required). Validators can also be chained, like email:email:required. This property can be specified on multiple lines.',
              'area_trans' => '',
            ),
            'errTpl' => 
            array (
              'name' => 'errTpl',
              'desc' => 'prop_formit.errtpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<span class="error">[[+error]]</span>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The wrapper template for error messages.',
              'area_trans' => '',
            ),
            'validationErrorMessage' => 
            array (
              'name' => 'validationErrorMessage',
              'desc' => 'prop_formit.validationerrormessage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<p class="error">A form validation error occurred. Please check the values you have entered.</p>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A general error message to set to a placeholder if validation fails. Can contain [[+errors]] if you want to display a list of all errors at the top.',
              'area_trans' => '',
            ),
            'validationErrorBulkTpl' => 
            array (
              'name' => 'validationErrorBulkTpl',
              'desc' => 'prop_formit.validationerrorbulktpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<li>[[+error]]</li>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'HTML tpl that is used for each individual error in the generic validation error message value.',
              'area_trans' => '',
            ),
            'trimValuesBeforeValidation' => 
            array (
              'name' => 'trimValuesBeforeValidation',
              'desc' => 'prop_formit.trimvaluesdeforevalidation_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Whether or not to trim spaces from the beginning and end of values before attempting validation. Defaults to true.',
              'area_trans' => '',
            ),
            'customValidators' => 
            array (
              'name' => 'customValidators',
              'desc' => 'prop_formit.customvalidators_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A comma-separated list of custom validator names (snippets) you plan to use in this form. They must be explicitly stated here, or they will not be run.',
              'area_trans' => '',
            ),
            'clearFieldsOnSuccess' => 
            array (
              'name' => 'clearFieldsOnSuccess',
              'desc' => 'prop_formit.clearfieldsonsuccess_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If true, will clear the fields on a successful form submission that does not redirect.',
              'area_trans' => '',
            ),
            'successMessage' => 
            array (
              'name' => 'successMessage',
              'desc' => 'prop_formit.successmessage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If set, will set this a placeholder with the name of the value of the property &successMessagePlaceholder, which defaults to `fi.successMessage`.',
              'area_trans' => '',
            ),
            'successMessagePlaceholder' => 
            array (
              'name' => 'successMessagePlaceholder',
              'desc' => 'prop_formit.successmessageplaceholder_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'fi.successMessage',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The placeholder to set the success message to.',
              'area_trans' => '',
            ),
            'store' => 
            array (
              'name' => 'store',
              'desc' => 'prop_formit.store_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If true, will store the data in the cache for retrieval using the FormItRetriever snippet.',
              'area_trans' => '',
            ),
            'placeholderPrefix' => 
            array (
              'name' => 'placeholderPrefix',
              'desc' => 'prop_formit.placeholderprefix_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'fi.',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The prefix to use for all placeholders set by FormIt for fields. Defaults to `fi.`',
              'area_trans' => '',
            ),
            'storeTime' => 
            array (
              'name' => 'storeTime',
              'desc' => 'prop_formit.storetime_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 300,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `store` is set to true, this specifies the number of seconds to store the data from the form submission. Defaults to five minutes.',
              'area_trans' => '',
            ),
            'allowFiles' => 
            array (
              'name' => 'allowFiles',
              'desc' => 'prop_formit.allowfiles_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If set to 0, will prevent files from being submitted on the form.',
              'area_trans' => '',
            ),
            'spamEmailFields' => 
            array (
              'name' => 'spamEmailFields',
              'desc' => 'prop_formit.spamemailfields_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'email',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `spam` is set as a hook, a comma-separated list of fields containing emails to check spam against.',
              'area_trans' => '',
            ),
            'spamCheckIp' => 
            array (
              'name' => 'spamCheckIp',
              'desc' => 'prop_formit.spamcheckip_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `spam` is set as a hook, and this is true, will check the IP as well.',
              'area_trans' => '',
            ),
            'recaptchaJs' => 
            array (
              'name' => 'recaptchaJs',
              'desc' => 'prop_formit.recaptchajs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '{}',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `recaptcha` is set as a hook, this can be a JSON object that will be set to the JS RecaptchaOptions variable, which configures options for reCaptcha.',
              'area_trans' => '',
            ),
            'recaptchaTheme' => 
            array (
              'name' => 'recaptchaTheme',
              'desc' => 'prop_formit.recaptchatheme_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'formit.opt_red',
                  'value' => 'red',
                  'name' => 'Red',
                ),
                1 => 
                array (
                  'text' => 'formit.opt_white',
                  'value' => 'white',
                  'name' => 'White',
                ),
                2 => 
                array (
                  'text' => 'formit.opt_clean',
                  'value' => 'clean',
                  'name' => 'Clean',
                ),
                3 => 
                array (
                  'text' => 'formit.opt_blackglass',
                  'value' => 'blackglass',
                  'name' => 'Black Glass',
                ),
              ),
              'value' => 'clean',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `recaptcha` is set as a hook, this will select a theme for the reCaptcha widget.',
              'area_trans' => '',
            ),
            'redirectTo' => 
            array (
              'name' => 'redirectTo',
              'desc' => 'prop_formit.redirectto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `redirect` is set as a hook, this must specify the Resource ID to redirect to.',
              'area_trans' => '',
            ),
            'redirectParams' => 
            array (
              'name' => 'redirectParams',
              'desc' => 'prop_formit.redirectparams_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A JSON array of parameters to pass to the redirect hook that will be passed when redirecting.',
              'area_trans' => '',
            ),
            'emailTo' => 
            array (
              'name' => 'emailTo',
              'desc' => 'prop_formit.emailto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `email` is set as a hook, then this specifies the email(s) to send the email to. Can be a comma-separated list of email addresses.',
              'area_trans' => '',
            ),
            'emailToName' => 
            array (
              'name' => 'emailToName',
              'desc' => 'prop_formit.emailtoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailTo` property.',
              'area_trans' => '',
            ),
            'emailFrom' => 
            array (
              'name' => 'emailFrom',
              'desc' => 'prop_formit.emailfrom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the From: address for the email. If not set, will first look for an `email` form field. If none is found, will default to the `emailsender` system setting.',
              'area_trans' => '',
            ),
            'emailFromName' => 
            array (
              'name' => 'emailFromName',
              'desc' => 'prop_formit.emailfromname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the From: name for the email.',
              'area_trans' => '',
            ),
            'emailReplyTo' => 
            array (
              'name' => 'emailReplyTo',
              'desc' => 'prop_formit.emailreplyto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the Reply-To: address for the email.',
              'area_trans' => '',
            ),
            'emailReplyToName' => 
            array (
              'name' => 'emailReplyToName',
              'desc' => 'prop_formit.emailreplytoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the Reply-To: name for the email.',
              'area_trans' => '',
            ),
            'emailCC' => 
            array (
              'name' => 'emailCC',
              'desc' => 'prop_formit.emailcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `email` is set as a hook, then this specifies the email(s) to send the email to as a CC. Can be a comma-separated list of email addresses.',
              'area_trans' => '',
            ),
            'emailCCName' => 
            array (
              'name' => 'emailCCName',
              'desc' => 'prop_formit.emailccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailCC` property.',
              'area_trans' => '',
            ),
            'emailBCC' => 
            array (
              'name' => 'emailBCC',
              'desc' => 'prop_formit.emailbcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `email` is set as a hook, then this specifies the email(s) to send the email to as a BCC. Can be a comma-separated list of email addresses.',
              'area_trans' => '',
            ),
            'emailBCCName' => 
            array (
              'name' => 'emailBCCName',
              'desc' => 'prop_formit.emailbccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailBCC` property.',
              'area_trans' => '',
            ),
            'emailSubject' => 
            array (
              'name' => 'emailSubject',
              'desc' => 'prop_formit.emailsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `email` is set as a hook, this is required as a subject line for the email.',
              'area_trans' => '',
            ),
            'emailUseFieldForSubject' => 
            array (
              'name' => 'emailUseFieldForSubject',
              'desc' => 'prop_formit.emailusefieldforsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If the field `subject` is passed into the form, if this is true, it will use the field content for the subject line of the email.',
              'area_trans' => '',
            ),
            'emailHtml' => 
            array (
              'name' => 'emailHtml',
              'desc' => 'prop_formit.emailhtml_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, this toggles HTML emails or not. Defaults to true.',
              'area_trans' => '',
            ),
            'emailConvertNewlines' => 
            array (
              'name' => 'emailConvertNewlines',
              'desc' => 'prop_formit.emailconvertnewlines_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If true and emailHtml is set to 1, will convert newlines to BR tags in the email.',
              'area_trans' => '',
            ),
            'emailMultiWrapper' => 
            array (
              'name' => 'emailMultiWrapper',
              'desc' => 'prop_formit.emailmultiwrapper_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '[[+value]]',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Will wrap each item in a collection of fields sent via checkboxes/multi-selects. Defaults to just the value.',
              'area_trans' => '',
            ),
            'emailMultiSeparator' => 
            array (
              'name' => 'emailMultiSeparator',
              'desc' => 'prop_formit.emailmultiseparator_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.emailmultiseparator_desc',
              'area_trans' => '',
            ),
            'fiarTpl' => 
            array (
              'name' => 'fiarTpl',
              'desc' => 'prop_fiar.fiartpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `FormItAutoResponder` is set as a hook, then this specifies auto-response template to send as the email.',
              'area_trans' => '',
            ),
            'fiarToField' => 
            array (
              'name' => 'fiarToField',
              'desc' => 'prop_fiar.fiartofield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'email',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `FormItAutoResponder` is set as a hook, then this specifies which form field shall be used for the To: address in the auto-response email.',
              'area_trans' => '',
            ),
            'fiarSubject' => 
            array (
              'name' => 'fiarSubject',
              'desc' => 'prop_fiar.fiarsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '[[++site_name]] Auto-Responder',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `FormItAutoResponder` is set as a hook, this is required as a subject line for the email.',
              'area_trans' => '',
            ),
            'fiarFrom' => 
            array (
              'name' => 'fiarFrom',
              'desc' => 'prop_fiar.fiarfrom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `FormItAutoResponder` is set as a hook, and this is set, will specify the From: address for the email. If not set, will first look for an `email` form field. If none is found, will default to the `emailsender` system setting.',
              'area_trans' => '',
            ),
            'fiarFromName' => 
            array (
              'name' => 'fiarFromName',
              'desc' => 'prop_fiar.fiarfromname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `FormItAutoResponder` is set as a hook, and this is set, will specify the From: name for the email.',
              'area_trans' => '',
            ),
            'fiarReplyTo' => 
            array (
              'name' => 'fiarReplyTo',
              'desc' => 'prop_fiar.fiarreplyto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `FormItAutoResponder` is set as a hook, and this is set, will specify the Reply-To: address for the email.',
              'area_trans' => '',
            ),
            'fiarReplyToName' => 
            array (
              'name' => 'fiarReplyToName',
              'desc' => 'prop_fiar.fiarreplytoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `FormItAutoResponder` is set as a hook, and this is set, will specify the Reply-To: name for the email.',
              'area_trans' => '',
            ),
            'fiarCC' => 
            array (
              'name' => 'fiarCC',
              'desc' => 'prop_fiar.fiarcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `FormItAutoResponder` is set as a hook, then this specifies the email(s) to send the email to as a CC. Can be a comma-separated list of email addresses.',
              'area_trans' => '',
            ),
            'fiarCCName' => 
            array (
              'name' => 'fiarCCName',
              'desc' => 'prop_fiar.fiarccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `FormItAutoResponder` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailCC` property.',
              'area_trans' => '',
            ),
            'fiarBCC' => 
            array (
              'name' => 'fiarBCC',
              'desc' => 'prop_fiar.fiarbcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `FormItAutoResponder` is set as a hook, then this specifies the email(s) to send the email to as a BCC. Can be a comma-separated list of email addresses.',
              'area_trans' => '',
            ),
            'fiarBCCName' => 
            array (
              'name' => 'fiarBCCName',
              'desc' => 'prop_fiar.fiarbccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `FormItAutoResponder` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailBCC` property.',
              'area_trans' => '',
            ),
            'fiarHtml' => 
            array (
              'name' => 'fiarHtml',
              'desc' => 'prop_fiar.fiarhtml_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `FormItAutoResponder` is set as a hook, this toggles HTML emails or not. Defaults to true.',
              'area_trans' => '',
            ),
            'mathMinRange' => 
            array (
              'name' => 'mathMinRange',
              'desc' => 'prop_math.mathminrange_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 10,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `math` is set as a hook, the minimum range for each number in the equation.',
              'area_trans' => '',
            ),
            'mathMaxRange' => 
            array (
              'name' => 'mathMaxRange',
              'desc' => 'prop_math.mathmaxrange_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 100,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `math` is set as a hook, the maximum range for each number in the equation.',
              'area_trans' => '',
            ),
            'mathField' => 
            array (
              'name' => 'mathField',
              'desc' => 'prop_math.mathfield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'math',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `math` is set as a hook, the name of the input field for the answer.',
              'area_trans' => '',
            ),
            'mathOp1Field' => 
            array (
              'name' => 'mathOp1Field',
              'desc' => 'prop_math.mathop1field_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'op1',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `math` is set as a hook, the name of the field for the 1st number in the equation.',
              'area_trans' => '',
            ),
            'mathOp2Field' => 
            array (
              'name' => 'mathOp2Field',
              'desc' => 'prop_math.mathop2field_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'op2',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `math` is set as a hook, the name of the field for the 2nd number in the equation.',
              'area_trans' => '',
            ),
            'mathOperatorField' => 
            array (
              'name' => 'mathOperatorField',
              'desc' => 'prop_math.mathoperatorfield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'operator',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `math` is set as a hook, the name of the field for the operator in the equation.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * FormIt
 *
 * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>
 *
 * FormIt is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package formit
 */
/**
 * FormIt
 *
 * A dynamic form processing Snippet for MODx Revolution.
 *
 * @package formit
 */
require_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\',null,MODX_CORE_PATH).\'components/formit/\').\'model/formit/formit.class.php\';
$fi = new FormIt($modx,$scriptProperties);
$fi->initialize($modx->context->get(\'key\'));
$fi->loadRequest();

$fields = $fi->request->prepare();
return $fi->request->handle($fields);',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'StoreLocator' => 
      array (
        'fields' => 
        array (
          'id' => 47,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'StoreLocator',
          'description' => 'StoreLocator\'s main snippet',
          'editor_type' => 0,
          'category' => 16,
          'cache_type' => 0,
          'snippet' => '/**
 * StoreLocator
 *
 * Copyright 2011-12 by SCHERP Ontwikkeling <info@scherpontwikkeling.nl>
 *
 * This file is part of StoreLocator.
 *
 * StoreLocator is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * StoreLocator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * StoreLocator; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package StoreLocator
 */
// Load the userValueList class
$storeLocator = $modx->getService(\'storelocator\',\'StoreLocator\', $modx->getOption(\'storelocator.core_path\', null, $modx->getOption(\'core_path\').\'components/storelocator/\').\'model/storelocator/\', $scriptProperties);
if (!($storeLocator instanceof StoreLocator)) return \'\';

// Configuration parameters
$apiKey = $modx->getOption(\'apiKey\', $scriptProperties, $modx->getOption(\'storelocator.apiKey\'));
$zoom = $modx->getOption(\'zoom\', $scriptProperties, 8);
$storeZoom = $modx->getOption(\'storeZoom\', $scriptProperties, 13);
$searchZoom = $modx->getOption(\'searchZoom\', $scriptProperties, 13);
$width = $modx->getOption(\'width\', $scriptProperties, 300);
$height = $modx->getOption(\'height\', $scriptProperties, 400);
$mapType = $modx->getOption(\'mapType\', $scriptProperties, \'ROADMAP\');
$defaultRadius = $modx->getOption(\'defaultRadius\', $scriptProperties, 5);
$centerLongitude = $modx->getOption(\'centerLongitude\', $scriptProperties, 6.61480);
$centerLatitude = $modx->getOption(\'centerLatitude\', $scriptProperties, 52.40441);
$markerImage = $modx->getOption(\'markerImage\', $scriptProperties, \'0\');
$sortDir = $modx->getOption(\'sortDir\', $scriptProperties, \'ASC\');
$limit = $modx->getOption(\'limit\', $scriptProperties, 0);

// Templating parameters
$formTpl = $modx->getOption(\'formTpl\', $scriptProperties, \'sl.form\');
$storeRowTpl = $modx->getOption(\'storeRowTpl\', $scriptProperties, \'sl.storerow\');
$storeInfoWindowTpl = $modx->getOption(\'storeInfoWindowTpl\', $scriptProperties, \'sl.infowindow\');
$noResultsTpl = $modx->getOption(\'noResultsTpl\', $scriptProperties, \'sl.noresultstpl\');

// Developers templating parameters
$scriptWrapperTpl = $modx->getOption(\'scriptWrapperTpl\', $scriptProperties, \'sl.scriptwrapper\');
$scriptStoreMarker = $modx->getOption(\'scriptStoreMarker\', $scriptProperties, \'sl.scriptstoremarker\');

// Load lexicon
$modx->lexicon->load(\'storelocator:frontend\');

// Register the google maps API
if ($apiKey != \'\') {
	$modx->regClientStartupScript(\'http://maps.googleapis.com/maps/api/js?sensor=false&key=\'.$apiKey);
} else {
	$modx->regClientStartupScript(\'http://maps.googleapis.com/maps/api/js?sensor=false\');
}

// The init code

$centerLongitude = isset($_REQUEST[\'longitude\']) ? floatval($_REQUEST[\'longitude\']) : $centerLongitude;
$centerLatitude = isset($_REQUEST[\'latitude\']) ? floatval($_REQUEST[\'latitude\']) : $centerLatitude;
$zoom = isset($_REQUEST[\'longitude\']) ? $searchZoom : $zoom;
$modx->regClientStartupHTMLBlock($storeLocator->getChunk($scriptWrapperTpl, array(
	\'centerLatitude\' => $centerLatitude,
	\'centerLongitude\' => $centerLongitude,
	\'zoom\' => $zoom,
	\'mapType\' => $mapType
)));

// Parse store chunks
$query = $modx->newQuery(\'slStore\'); 
$query->sortby(\'sort\', $sortDir);
$query->limit($limit);

$totalStores = $modx->getCount(\'slStore\', $query);
$stores = $modx->getCollection(\'slStore\', $query);
$storeListOutput = \'\';
$i = 0;
$matchedStores = 0;
foreach($stores as $store) {

	if (strtolower($_SERVER[\'REQUEST_METHOD\']) == \'post\') {
		$longitude = floatval($_REQUEST[\'longitude\']);
		$latitude = floatval($_REQUEST[\'latitude\']);
		$distance = (int) $_REQUEST[\'radius\'];
		
		list($lat1, $lat2, $lon1, $lon2) = $storeLocator->getBoundingBox($latitude, $longitude, $distance);
			
		// Check if this store is within the selected radius
		if ($store->get(\'longitude\') > $lon1 and $store->get(\'longitude\') < $lon2 && $store->get(\'latitude\') > $lat1 && $store->get(\'latitude\') < $lat2) {
			// All is okay
		} else {
			// This store is not within the radius
			continue;
		}
	}

	// Get resource that belongs to the store
	$resource = $modx->getObject(\'modResource\', $store->get(\'resource_id\'));
	
	// If the resource doesn\'t exist just skip it
	if ($resource != null) {
		$resourceArray = $resource->toArray();
		$storeListOutput .= $storeLocator->getChunk($storeRowTpl, array_merge(
			$resourceArray,
			array(
				\'store\' => $store->toArray(),
				\'totalStores\' => $totalStores,
				\'onclick\' => \'storeLocatorMap.setCenter(new google.maps.LatLng(\'.$store->get(\'latitude\').\', \'.$store->get(\'longitude\').\')); storeLocatorMap.setZoom(\'.$storeZoom.\');\'
			)
		));
		
		$infoWindowOutput = \'\';
		$infoWindowOutput = $storeLocator->getChunk($storeInfoWindowTpl, array_merge(
			$resourceArray,
			array(
				\'store\' => $store->toArray(),
				\'totalStores\' => $totalStores
			)
		));
		
		$storeListOutput .= $storeLocator->getChunk($scriptStoreMarker, array_merge(
			$resourceArray,
			array(
				\'store\' => $store->toArray(),
				\'content\' => $infoWindowOutput,
				\'markerImage\' => $markerImage
			)
		));
		
		$i++;
		$matchedStores++;
	}
}
 
// Nothing is found
if ($i == 0) {
	$storeListOutput = $storeLocator->getChunk($noResultsTpl);
}

$locatorFormOutput = $storeLocator->getChunk($formTpl, array(
	\'query\' => str_replace(array(\'[\', \']\'), \'\', $_REQUEST[\'query\']),
	\'radius\' => isset($_REQUEST[\'radius\']) ? $_REQUEST[\'radius\'] : $defaultRadius,
	\'totalStores\' => $totalStores,
	\'matchedStores\' => $matchedStores
));

// Parse output to place holders
$modx->toPlaceHolders(array(
	\'map\' => \'<div id="storelocator_canvas" style="width: \'.$width.\'px; height: \'.$height.\'px;"></div>\',
	\'storeList\' => $storeListOutput,
	\'form\' => $locatorFormOutput,
	\'totalStores\' => $totalStores,
	\'matchedStores\' => $matchedStores
), \'StoreLocator\');',
          'locked' => false,
          'properties' => 
          array (
            'apiKey' => 
            array (
              'name' => 'apiKey',
              'desc' => 'storelocator.prop_apikey_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Your Google Maps API key (not required)',
              'area_trans' => '',
            ),
            'zoom' => 
            array (
              'name' => 'zoom',
              'desc' => 'storelocator.prop_zoom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '8',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Standard zoom level when the map initializes',
              'area_trans' => '',
            ),
            'storeZoom' => 
            array (
              'name' => 'storeZoom',
              'desc' => 'storelocator.prop_storezoom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '13',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Zoom level when a user clicks on a store in the list',
              'area_trans' => '',
            ),
            'searchZoom' => 
            array (
              'name' => 'searchZoom',
              'desc' => 'storelocator.prop_searchzoom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '13',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Zoom level when user has searched and map is centered on their address',
              'area_trans' => '',
            ),
            'width' => 
            array (
              'name' => 'width',
              'desc' => 'storelocator.prop_width_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '300',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Width of the map',
              'area_trans' => '',
            ),
            'height' => 
            array (
              'name' => 'height',
              'desc' => 'storelocator.prop_height_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '400',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Height of the map',
              'area_trans' => '',
            ),
            'mapType' => 
            array (
              'name' => 'mapType',
              'desc' => 'storelocator.prop_maptype_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'storelocator.hybrid',
                  'value' => 'HYBRID',
                  'name' => 'This map type displays a transparent layer of major streets on satellite images',
                ),
                1 => 
                array (
                  'text' => 'storelocator.roadmap',
                  'value' => 'ROADMAP',
                  'name' => 'This map type displays a normal street map',
                ),
                2 => 
                array (
                  'text' => 'storelocator.satellite',
                  'value' => 'SATELLITE',
                  'name' => 'This map type displays satellite images',
                ),
                3 => 
                array (
                  'text' => 'storelocator.terrain',
                  'value' => 'TERRAIN',
                  'name' => 'This map type displays maps with physical features such as terrain and vegetation',
                ),
              ),
              'value' => 'ROADMAP',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'The type of the Google Map',
              'area_trans' => '',
            ),
            'defaultRadius' => 
            array (
              'name' => 'defaultRadius',
              'desc' => 'storelocator.prop_defaultradius_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '5',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'The default radius that will be selected in the search form',
              'area_trans' => '',
            ),
            'centerLongitude' => 
            array (
              'name' => 'centerLongitude',
              'desc' => 'storelocator.prop_centerlongitude_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '6.61480',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Longitude on which the map will center by default',
              'area_trans' => '',
            ),
            'centerLatitude' => 
            array (
              'name' => 'centerLatitude',
              'desc' => 'storelocator.prop_centerlatitude_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '52.40441',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Latitude on which the map will center by default',
              'area_trans' => '',
            ),
            'markerImage' => 
            array (
              'name' => 'markerImage',
              'desc' => 'storelocator.prop_markerimage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'A URL to an image to be used instead of the default Google Map marker',
              'area_trans' => '',
            ),
            'sortDir' => 
            array (
              'name' => 'sortDir',
              'desc' => 'storelocator.prop_sortdir_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'storelocator.asc',
                  'value' => 'ASC',
                  'name' => 'Sort ascending',
                ),
                1 => 
                array (
                  'text' => 'storelocator.desc',
                  'value' => 'DESC',
                  'name' => 'Sort descending',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Sort direction of the store list',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'storelocator.prop_limit_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'Maximum amount of stores shown by default and in search results',
              'area_trans' => '',
            ),
            'formTpl' => 
            array (
              'name' => 'formTpl',
              'desc' => 'storelocator.prop_formtpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'sl.form',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'The chunk for the form',
              'area_trans' => '',
            ),
            'storeRowTpl' => 
            array (
              'name' => 'storeRowTpl',
              'desc' => 'storelocator.prop_storerowtpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'sl.storerow',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'The chunk for a store row as shown in the list and search results',
              'area_trans' => '',
            ),
            'storeInfoWindowTpl' => 
            array (
              'name' => 'storeInfoWindowTpl',
              'desc' => 'storelocator.prop_storeinfowindowtpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'sl.infowindow',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'The chunk for the info window popup when clicked on a store marker',
              'area_trans' => '',
            ),
            'noResultsTpl' => 
            array (
              'name' => 'noResultsTpl',
              'desc' => 'storelocator.prop_noresultstpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'sl.noresultstpl',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'The chunk that shows when no results are found',
              'area_trans' => '',
            ),
            'scriptWrapperTpl' => 
            array (
              'name' => 'scriptWrapperTpl',
              'desc' => 'storelocator.prop_scriptwrappertpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'sl.scriptwrapper',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'The script wrapper (only change when you know what you\'re doing)',
              'area_trans' => '',
            ),
            'scriptStoreMarker' => 
            array (
              'name' => 'scriptStoreMarker',
              'desc' => 'storelocator.prop_scriptstoremarker_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'sl.scriptstoremarker',
              'lexicon' => 'storelocator:properties',
              'area' => '',
              'desc_trans' => 'The script store marker (only change when you know what you\'re doing)',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * StoreLocator
 *
 * Copyright 2011-12 by SCHERP Ontwikkeling <info@scherpontwikkeling.nl>
 *
 * This file is part of StoreLocator.
 *
 * StoreLocator is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * StoreLocator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * StoreLocator; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package StoreLocator
 */
// Load the userValueList class
$storeLocator = $modx->getService(\'storelocator\',\'StoreLocator\', $modx->getOption(\'storelocator.core_path\', null, $modx->getOption(\'core_path\').\'components/storelocator/\').\'model/storelocator/\', $scriptProperties);
if (!($storeLocator instanceof StoreLocator)) return \'\';

// Configuration parameters
$apiKey = $modx->getOption(\'apiKey\', $scriptProperties, $modx->getOption(\'storelocator.apiKey\'));
$zoom = $modx->getOption(\'zoom\', $scriptProperties, 8);
$storeZoom = $modx->getOption(\'storeZoom\', $scriptProperties, 13);
$searchZoom = $modx->getOption(\'searchZoom\', $scriptProperties, 13);
$width = $modx->getOption(\'width\', $scriptProperties, 300);
$height = $modx->getOption(\'height\', $scriptProperties, 400);
$mapType = $modx->getOption(\'mapType\', $scriptProperties, \'ROADMAP\');
$defaultRadius = $modx->getOption(\'defaultRadius\', $scriptProperties, 5);
$centerLongitude = $modx->getOption(\'centerLongitude\', $scriptProperties, 6.61480);
$centerLatitude = $modx->getOption(\'centerLatitude\', $scriptProperties, 52.40441);
$markerImage = $modx->getOption(\'markerImage\', $scriptProperties, \'0\');
$sortDir = $modx->getOption(\'sortDir\', $scriptProperties, \'ASC\');
$limit = $modx->getOption(\'limit\', $scriptProperties, 0);

// Templating parameters
$formTpl = $modx->getOption(\'formTpl\', $scriptProperties, \'sl.form\');
$storeRowTpl = $modx->getOption(\'storeRowTpl\', $scriptProperties, \'sl.storerow\');
$storeInfoWindowTpl = $modx->getOption(\'storeInfoWindowTpl\', $scriptProperties, \'sl.infowindow\');
$noResultsTpl = $modx->getOption(\'noResultsTpl\', $scriptProperties, \'sl.noresultstpl\');

// Developers templating parameters
$scriptWrapperTpl = $modx->getOption(\'scriptWrapperTpl\', $scriptProperties, \'sl.scriptwrapper\');
$scriptStoreMarker = $modx->getOption(\'scriptStoreMarker\', $scriptProperties, \'sl.scriptstoremarker\');

// Load lexicon
$modx->lexicon->load(\'storelocator:frontend\');

// Register the google maps API
if ($apiKey != \'\') {
	$modx->regClientStartupScript(\'http://maps.googleapis.com/maps/api/js?sensor=false&key=\'.$apiKey);
} else {
	$modx->regClientStartupScript(\'http://maps.googleapis.com/maps/api/js?sensor=false\');
}

// The init code

$centerLongitude = isset($_REQUEST[\'longitude\']) ? floatval($_REQUEST[\'longitude\']) : $centerLongitude;
$centerLatitude = isset($_REQUEST[\'latitude\']) ? floatval($_REQUEST[\'latitude\']) : $centerLatitude;
$zoom = isset($_REQUEST[\'longitude\']) ? $searchZoom : $zoom;
$modx->regClientStartupHTMLBlock($storeLocator->getChunk($scriptWrapperTpl, array(
	\'centerLatitude\' => $centerLatitude,
	\'centerLongitude\' => $centerLongitude,
	\'zoom\' => $zoom,
	\'mapType\' => $mapType
)));

// Parse store chunks
$query = $modx->newQuery(\'slStore\'); 
$query->sortby(\'sort\', $sortDir);
$query->limit($limit);

$totalStores = $modx->getCount(\'slStore\', $query);
$stores = $modx->getCollection(\'slStore\', $query);
$storeListOutput = \'\';
$i = 0;
$matchedStores = 0;
foreach($stores as $store) {

	if (strtolower($_SERVER[\'REQUEST_METHOD\']) == \'post\') {
		$longitude = floatval($_REQUEST[\'longitude\']);
		$latitude = floatval($_REQUEST[\'latitude\']);
		$distance = (int) $_REQUEST[\'radius\'];
		
		list($lat1, $lat2, $lon1, $lon2) = $storeLocator->getBoundingBox($latitude, $longitude, $distance);
			
		// Check if this store is within the selected radius
		if ($store->get(\'longitude\') > $lon1 and $store->get(\'longitude\') < $lon2 && $store->get(\'latitude\') > $lat1 && $store->get(\'latitude\') < $lat2) {
			// All is okay
		} else {
			// This store is not within the radius
			continue;
		}
	}

	// Get resource that belongs to the store
	$resource = $modx->getObject(\'modResource\', $store->get(\'resource_id\'));
	
	// If the resource doesn\'t exist just skip it
	if ($resource != null) {
		$resourceArray = $resource->toArray();
		$storeListOutput .= $storeLocator->getChunk($storeRowTpl, array_merge(
			$resourceArray,
			array(
				\'store\' => $store->toArray(),
				\'totalStores\' => $totalStores,
				\'onclick\' => \'storeLocatorMap.setCenter(new google.maps.LatLng(\'.$store->get(\'latitude\').\', \'.$store->get(\'longitude\').\')); storeLocatorMap.setZoom(\'.$storeZoom.\');\'
			)
		));
		
		$infoWindowOutput = \'\';
		$infoWindowOutput = $storeLocator->getChunk($storeInfoWindowTpl, array_merge(
			$resourceArray,
			array(
				\'store\' => $store->toArray(),
				\'totalStores\' => $totalStores
			)
		));
		
		$storeListOutput .= $storeLocator->getChunk($scriptStoreMarker, array_merge(
			$resourceArray,
			array(
				\'store\' => $store->toArray(),
				\'content\' => $infoWindowOutput,
				\'markerImage\' => $markerImage
			)
		));
		
		$i++;
		$matchedStores++;
	}
}
 
// Nothing is found
if ($i == 0) {
	$storeListOutput = $storeLocator->getChunk($noResultsTpl);
}

$locatorFormOutput = $storeLocator->getChunk($formTpl, array(
	\'query\' => str_replace(array(\'[\', \']\'), \'\', $_REQUEST[\'query\']),
	\'radius\' => isset($_REQUEST[\'radius\']) ? $_REQUEST[\'radius\'] : $defaultRadius,
	\'totalStores\' => $totalStores,
	\'matchedStores\' => $matchedStores
));

// Parse output to place holders
$modx->toPlaceHolders(array(
	\'map\' => \'<div id="storelocator_canvas" style="width: \'.$width.\'px; height: \'.$height.\'px;"></div>\',
	\'storeList\' => $storeListOutput,
	\'form\' => $locatorFormOutput,
	\'totalStores\' => $totalStores,
	\'matchedStores\' => $matchedStores
), \'StoreLocator\');',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'getResourceField' => 
      array (
        'fields' => 
        array (
          'id' => 39,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'getResourceField',
          'description' => 'Grab a value from a resource field or a TV',
          'editor_type' => 0,
          'category' => 7,
          'cache_type' => 0,
          'snippet' => '/**
 *
 * getResourceField
 *
 * A snippet to grab a value from a resource field or a TV
 *
 * @ author Paul Merchant
 * @ author Shaun McCormick
 * @ copyright 2011 Paul Merchant
 * @ version 1.0.3 - August 8, 2011
 * @ MIT License
 *
 * OPTIONS
 * id - The resource ID
 * field - (Opt) The field or template variable name, defaults to pagetitle
 * isTV - (Opt) Set as true to return a raw template variable
 * processTV - (Opt) Set as true to return a rendered template variable
 * default - (Opt) The value returned if no field is found
 *
 * Exmaple1: [[getResourceField? &id=`123`]]
 * Example2: [[getResourceField? &id=`[[UltimateParent?]]` &field=`myTV` &isTV=`1`]]
 * Example3: [[getResourceField? &id=`[[*parent]]` &field=`myTV` &processTV=`1`]]
 *
**/

// set defaults
$id = $modx->getOption(\'id\',$scriptProperties,$modx->resource->get(\'id\'));
$field = $modx->getOption(\'field\',$scriptProperties,\'pagetitle\');
$isTV = $modx->getOption(\'isTV\',$scriptProperties,false);
$processTV = $modx->getOption(\'processTV\',$scriptProperties,false);
$output = $modx->getOption(\'default\',$scriptProperties,\'\');

if ($isTV || $processTV) {
    // get the tv object
    $tv = $modx->getObject(\'modTemplateVar\',array(\'name\'=>$field));
    if (empty($tv)) return $output;
    if ($processTV) {
        // get rendered tv value
        $tvValue = $tv->renderOutput($id);
    } else {
        // get raw tv value
        $tvValue = $tv->getValue($id);
    }
    if ($tvValue !== null) {
        $output = $tvValue;
    }
} else {
    if ($id == $modx->resource->get(\'id\')) {
        // use the current resource
        $resource =& $modx->resource;
        // current resource can infinite loop if pulling content field into itself
        if ($field == \'content\') {
            return $output;
        }
    } else {
        // grab only the columns we need
        $criteria = $modx->newQuery(\'modResource\');
        $criteria->select($modx->getSelectColumns(\'modResource\',\'modResource\',\'\',array(\'id\',$field)));
        $criteria->where(array(\'id\'=>$id,));
        $resource = $modx->getObject(\'modResource\',$criteria);
        if (empty($resource)) return $output;
    }
    $fieldValue = $resource->get($field);
    if ($fieldValue !== null) {
        $output = $fieldValue;
    }
}

return $output;',
          'locked' => false,
          'properties' => 
          array (
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 *
 * getResourceField
 *
 * A snippet to grab a value from a resource field or a TV
 *
 * @ author Paul Merchant
 * @ author Shaun McCormick
 * @ copyright 2011 Paul Merchant
 * @ version 1.0.3 - August 8, 2011
 * @ MIT License
 *
 * OPTIONS
 * id - The resource ID
 * field - (Opt) The field or template variable name, defaults to pagetitle
 * isTV - (Opt) Set as true to return a raw template variable
 * processTV - (Opt) Set as true to return a rendered template variable
 * default - (Opt) The value returned if no field is found
 *
 * Exmaple1: [[getResourceField? &id=`123`]]
 * Example2: [[getResourceField? &id=`[[UltimateParent?]]` &field=`myTV` &isTV=`1`]]
 * Example3: [[getResourceField? &id=`[[*parent]]` &field=`myTV` &processTV=`1`]]
 *
**/

// set defaults
$id = $modx->getOption(\'id\',$scriptProperties,$modx->resource->get(\'id\'));
$field = $modx->getOption(\'field\',$scriptProperties,\'pagetitle\');
$isTV = $modx->getOption(\'isTV\',$scriptProperties,false);
$processTV = $modx->getOption(\'processTV\',$scriptProperties,false);
$output = $modx->getOption(\'default\',$scriptProperties,\'\');

if ($isTV || $processTV) {
    // get the tv object
    $tv = $modx->getObject(\'modTemplateVar\',array(\'name\'=>$field));
    if (empty($tv)) return $output;
    if ($processTV) {
        // get rendered tv value
        $tvValue = $tv->renderOutput($id);
    } else {
        // get raw tv value
        $tvValue = $tv->getValue($id);
    }
    if ($tvValue !== null) {
        $output = $tvValue;
    }
} else {
    if ($id == $modx->resource->get(\'id\')) {
        // use the current resource
        $resource =& $modx->resource;
        // current resource can infinite loop if pulling content field into itself
        if ($field == \'content\') {
            return $output;
        }
    } else {
        // grab only the columns we need
        $criteria = $modx->newQuery(\'modResource\');
        $criteria->select($modx->getSelectColumns(\'modResource\',\'modResource\',\'\',array(\'id\',$field)));
        $criteria->where(array(\'id\'=>$id,));
        $resource = $modx->getObject(\'modResource\',$criteria);
        if (empty($resource)) return $output;
    }
    $fieldValue = $resource->get($field);
    if ($fieldValue !== null) {
        $output = $fieldValue;
    }
}

return $output;',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'StoreLocatorEscape' => 
      array (
        'fields' => 
        array (
          'id' => 48,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'StoreLocatorEscape',
          'description' => 'Internal snippet used with StoreLocator',
          'editor_type' => 0,
          'category' => 16,
          'cache_type' => 0,
          'snippet' => '/**
 * StoreLocator
 *
 * Copyright 2011-12 by SCHERP Ontwikkeling <info@scherpontwikkeling.nl>
 *
 * This file is part of StoreLocator.
 *
 * StoreLocator is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * StoreLocator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * StoreLocator; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package StoreLocator
 */
return rawurlencode($input);',
          'locked' => false,
          'properties' => NULL,
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * StoreLocator
 *
 * Copyright 2011-12 by SCHERP Ontwikkeling <info@scherpontwikkeling.nl>
 *
 * This file is part of StoreLocator.
 *
 * StoreLocator is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * StoreLocator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * StoreLocator; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package StoreLocator
 */
return rawurlencode($input);',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'Heading_title' => 
      array (
        'fields' => 
        array (
          'id' => 14,
          'source' => 0,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'Heading_title',
          'caption' => 'Banner Text',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'Left_content' => 
      array (
        'fields' => 
        array (
          'id' => 12,
          'source' => 0,
          'property_preprocess' => false,
          'type' => 'richtext',
          'name' => 'Left_content',
          'caption' => 'Page Left Column Content',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'Right_content' => 
      array (
        'fields' => 
        array (
          'id' => 13,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'richtext',
          'name' => 'Right_content',
          'caption' => 'Page Right Column Content',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);