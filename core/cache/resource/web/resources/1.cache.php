<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 1,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Home',
    'longtitle' => '',
    'description' => '',
    'alias' => 'index',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 4,
    'menuindex' => 0,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1406624572,
    'editedby' => 1,
    'editedon' => 1413993470,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 0,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => '',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'Tab_1' => 
    array (
      0 => 'Tab_1',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'text',
    ),
    'Heading_title' => 
    array (
      0 => 'Heading_title',
      1 => 'Getting better at what you do every day',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'Content_tab_1' => 
    array (
      0 => 'Content_tab_1',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'richtext',
    ),
    'Tab_2' => 
    array (
      0 => 'Tab_2',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'text',
    ),
    'Content_tab_2' => 
    array (
      0 => 'Content_tab_2',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'richtext',
    ),
    'Tab_3' => 
    array (
      0 => 'Tab_3',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'text',
    ),
    'Content_tab_3' => 
    array (
      0 => 'Content_tab_3',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'richtext',
    ),
    'Tab_4' => 
    array (
      0 => 'Tab_4',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'text',
    ),
    'Content_tab_4' => 
    array (
      0 => 'Content_tab_4',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'richtext',
    ),
    'Tab_5' => 
    array (
      0 => 'Tab_5',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'text',
    ),
    'Content_tab_5' => 
    array (
      0 => 'Content_tab_5',
      1 => '',
      2 => '',
      3 => NULL,
      4 => 'text',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NGA</title>

    <!-- Bootstrap -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         <ul class="nav"><li class="first active"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
         </div> </div>  
	<header id="main-header">
		<img id="header-banner" src="/assets/images/banner.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="index.php?id=8" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="http://nga.loyaltymatters.co.uk/"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             <ul class="nav"><li class="first active"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">Getting better at what you do every day </h1>
				<div class="img_man"><img src="/assets/images/man.png" class="img-responsive"/></div>
            </div>            
            <!-- Page Headline Start-->
        </div>

    </header>
    <!-- HEADER END->
    <!-- //Who are we// Block Start-->
    <div class="block_whoarewe">
    	<div class="container">
                <div class="col-lg-9 col-md-9 col-sm-8 description_text">Whether you are facing opportunities or challenges you’ve come to the right place</div>
                <div class="col-lg-3 col-md-3 col-sm-4"><a href="index.php?id=8" class="whoarewe_button"><i class="fa fa-hand-o-right"></i> Who we are</a></div> 
        </div>   
    </div>               
    <!-- //Who are we// Block End-->	

    
      <!-- WHO WE ARE BOX START-->
  	<div class="block_whoareyou">
    	<div class="container">
        	<div class="col-lg-4 col-md-4 col-sm-3  col-xs-2"><hr/></div>
            <div class="col-lg-4 col-md-4 col-sm-6  col-xs-8"><div class="panel-heading">Are you...</div></div>
            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-2"><hr/></div>
        </div>
        <div class="container">
        	<div class="col-lg-12">
                <div class="hidden-lg hidden-md hidden-sm"><div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          A Senior Executive?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/SE.jpg"/><p class="textRed">There is no doubt that as the world begins to transition out of global recession, a business leaders’ focus returns to growth. 
</p> 
    
   <p>The key questions are around how can you defend and grow your current markets whilst driving an innovative expansion strategy that secures top and bottom line?</p>
<p>If you are going to succeed, leadership across multiple levels of the organisation is critical. Everyone must know what they are doing, why they are doing it, and what it means for the organisation. If you had asked the parking attendant at NASA in the 1970’s “What do you do here?” His answer would have been simple… “We put people on the Moon.”</p>
<p>Does everyone in your organisation have such a clear understanding of the Vison, The Mission and the Values that get you all out of bed in the morning? </p>
<p>Does everyone in your organisation live by the ethos of100% accountability and zero excuses for not giving their best each and every day?</p>
<p>Is the story so compelling that you can attract and retain the right talent?</p>
<p>Is the story so compelling that you can seamlessly manage mergers and acquisitions?</p>
<p>Driving transition and leading cultural and organisational change is what NGA do best. Our whole reason for being is to help motivated accountable people secure growth whilst living to their corporate and personal values. </p>
<p>It’s what gets us out of bed in the morning!</p>
<p>If you would like to talk to other executives that have experienced working with NGA then contact us and we will put you in touch. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
         An MD / owner?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/MD.jpg"/><p class="textRed">You are unique!
</p> 


<p>Why? Well according to Forbes, 8 out of 10 entrepreneurs fail before they reach 18 months of being in business; and further research shows that nearly half of the 20% don’t make it past 5 years.</p>
<p>You however are different, and perhaps you are now in a place where it is time to invest in yourself, your leadership team and your people. </p>
<p>Developing your talent as you move from an ‘all hands on deck’ approach to a more structured process driven way of working is always a challenge. </p>
<p>Integrating new people, systems and processes can be difficult as you wrestle with the balance of what made you great as a start up with what will make you great as a sustainable business.</p>
<p>Whether consulting on process design, coaching and training your people managers, or working with your sales force to drive sales, NGA will take the time to understand your situation and provide the support you need for sustainable growth.</p>
<p>We are also able to give you a mirror to look in and a sounding board to challenge you with!</p>
<p>Contact us; we will come straight back to you.<br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          A HR or L&D Professional?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/HR.jpg"/><p class="textRed"> Modern business leaders see the value in HR and L&D departments, not just as a nice to have, but as a need to have in order to realise corporate goals. 
</p> 
    

<p>By utilising tools like business process redesign, organisational redesign, competency model development, and learning and development programmes, it is possible to effectively align human endeavour in the workplace with vision, mission and strategy.</p>
<p>The value of people is fully understood, and often times L&D have a reporting line to HR with the senior HRO sitting alongside the CEO and the rest of the executives.</p>
<p>That said your challenges may still be having a loud and large enough share of voice to influence wider operations within your business; It might still be a challenge to secure budget, and then identify tangible return on investment for your projects, and it is often a challenge to make sure initiatives have real teeth, enabling strategy implementation and not perceived as just tick box exercises.</p>
<p>NGA specialise in supporting HR/L&D functions in two ways</p>
<ol><li>You have done the analysis internally and want a partner to co-design and deliver a programme. During that process you want to be pushed back on and challenged in a way that develops your thinking and elaborates the solution.</li>
<li>You know you could get better at something and need some support in identifying the route cause and creating a sustainable solution to capitalise on the opportunity.</li></ol>
<p>We will help you deliver on your objectives, with tangible ROI, staged KPI metrics to measure impact, and regular feedback routes as the norm.</p>
<p>To find out more, contact us – We shall come straight back to you. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
          A Sales Leader?
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/SP.jpg"/><p class="textRed">Leading the sales force is not always the most rewarding job.
<p>You are held accountable when it goes wrong and not always recognised when it goes well. The tireless drive for consistency and equilibrium in forecasting and delivery is a challenge.</p>
<p>You have the time consuming discussions with marketing, regulatory affairs, IT, BI, production/manufacturing and other vital yet sometimes frustrating department heads,  not to mention the ‘old guard’ within your own team that are change resistant.</p>
<p>The key to success is an energised engaged sales force that has the right skillset, toolset and mind set to articulate the value proposition to the right people at the right time in the right way.</p>
<ul><li>Sales force automation can help, and is only as good as the people operating the systems. A pipeline /CRM tool only gets good Intel out if good Intel is put in. </li>
<li>Visibility of daily weekly activity that gives your leaders insight but does not detract from sales functionality is important to you.</li>
<li>An account development plan is often forgotten if it is not made into a living document by the people reviewing and coaching the plan.</li>
<li>Soft skills’ training is essential and will only work if the roles and responsibilities of sales people and sales managers is clearly defined, and clearly coached on a weekly basis.</li>
<li>An engaged CEO will help – how do you influence him/her and the other executives in a way that enables sales strategy?</li></ol>
<p>Sales Effectiveness is the heart of NGA’s core competence. It is where we came from and where we manage ourselves as a business. </p>
<p>We practice what we preach and what you see is what you get in terms of partnership. </p>
<p>Why not contact us to learn more about how we might work together? <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
          Developing yourself?
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/DY.jpg"/><p class="textRed">It doesn’t matter what stage of your career you are at, there is always the opportunity for personal development.
<p>In fact, you might as well replace (or intertwine) the word ‘career’ with the word ‘life’, as they are one and the same in many ways. What you become as a person will have a direct influence in how your life, and then your career develop. </p>
<p>The most common pitfall we come across is that people with busy lives overlook the need for deep reflection on what they do, how they do it and even what they think about. They find themselves wishing for things to be better rather than wishing for more skills. They all too easily complain about their environment but forget to complain about themselves.</p>
<p>At NGA we believe people are capable of the most incredible development given the right opportunity. We believe people care strongly about autonomy, mastery and purpose, and we know that if you have the right mind-set, with the right toolbox, and the right skillset, incredible things will happen.</p>
<p>Check out our <a href=“index.php?id=6”>Personal Development Members Club</a> and discover what you are capable off!</p>
<p>Or  drop us a mail? – We shall come straight back to you.
 <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
</div></div>
            	<!-- Nav tabs -->
                <div class="hidden-xs">
                <ul class="nav-tabs" role="tablist">
                  <li class="active"><a href="#senior-executive" role="tab" data-toggle="tab">A Senior<br/>
Executive?</a></li>
                  <li><a href="#owner" role="tab" data-toggle="tab">An MD /<br/>
owner?</a></li>
                  <li><a href="#hr-professional" role="tab" data-toggle="tab">HR or L&D <br/>
Professional?</a></li>
                  <li><a href="#sales-professional" role="tab" data-toggle="tab">A Sales <br/>
Leader?</a></li>
                  <li><a href="#developing-yourself" role="tab" data-toggle="tab">Developing <br/>
yourself?</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                      <div class="tab-pane active" id="senior-executive">
                      <img class="img-responsive alignLeft" src="/assets/images/SE.jpg"/><p class="textRed">There is no doubt that as the world begins to transition out of global recession, a business leaders’ focus returns to growth. 
</p> 
    
   <p>The key questions are around how can you defend and grow your current markets whilst driving an innovative expansion strategy that secures top and bottom line?</p>
<p>If you are going to succeed, leadership across multiple levels of the organisation is critical. Everyone must know what they are doing, why they are doing it, and what it means for the organisation. If you had asked the parking attendant at NASA in the 1970’s “What do you do here?” His answer would have been simple… “We put people on the Moon.”</p>
<p>Does everyone in your organisation have such a clear understanding of the Vison, The Mission and the Values that get you all out of bed in the morning? </p>
<p>Does everyone in your organisation live by the ethos of100% accountability and zero excuses for not giving their best each and every day?</p>
<p>Is the story so compelling that you can attract and retain the right talent?</p>
<p>Is the story so compelling that you can seamlessly manage mergers and acquisitions?</p>
<p>Driving transition and leading cultural and organisational change is what NGA do best. Our whole reason for being is to help motivated accountable people secure growth whilst living to their corporate and personal values. </p>
<p>It’s what gets us out of bed in the morning!</p>
<p>If you would like to talk to other executives that have experienced working with NGA then contact us and we will put you in touch. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="owner">
                      <img class="img-responsive alignLeft" src="/assets/images/MD.jpg"/><p class="textRed">You are unique!
</p> 


<p>Why? Well according to Forbes, 8 out of 10 entrepreneurs fail before they reach 18 months of being in business; and further research shows that nearly half of the 20% don’t make it past 5 years.</p>
<p>You however are different, and perhaps you are now in a place where it is time to invest in yourself, your leadership team and your people. </p>
<p>Developing your talent as you move from an ‘all hands on deck’ approach to a more structured process driven way of working is always a challenge. </p>
<p>Integrating new people, systems and processes can be difficult as you wrestle with the balance of what made you great as a start up with what will make you great as a sustainable business.</p>
<p>Whether consulting on process design, coaching and training your people managers, or working with your sales force to drive sales, NGA will take the time to understand your situation and provide the support you need for sustainable growth.</p>
<p>We are also able to give you a mirror to look in and a sounding board to challenge you with!</p>
<p>Contact us; we will come straight back to you.<br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="hr-professional">
                      <img class="img-responsive alignLeft" src="/assets/images/HR.jpg"/><p class="textRed"> Modern business leaders see the value in HR and L&D departments, not just as a nice to have, but as a need to have in order to realise corporate goals. 
</p> 
    

<p>By utilising tools like business process redesign, organisational redesign, competency model development, and learning and development programmes, it is possible to effectively align human endeavour in the workplace with vision, mission and strategy.</p>
<p>The value of people is fully understood, and often times L&D have a reporting line to HR with the senior HRO sitting alongside the CEO and the rest of the executives.</p>
<p>That said your challenges may still be having a loud and large enough share of voice to influence wider operations within your business; It might still be a challenge to secure budget, and then identify tangible return on investment for your projects, and it is often a challenge to make sure initiatives have real teeth, enabling strategy implementation and not perceived as just tick box exercises.</p>
<p>NGA specialise in supporting HR/L&D functions in two ways</p>
<ol><li>You have done the analysis internally and want a partner to co-design and deliver a programme. During that process you want to be pushed back on and challenged in a way that develops your thinking and elaborates the solution.</li>
<li>You know you could get better at something and need some support in identifying the route cause and creating a sustainable solution to capitalise on the opportunity.</li></ol>
<p>We will help you deliver on your objectives, with tangible ROI, staged KPI metrics to measure impact, and regular feedback routes as the norm.</p>
<p>To find out more, contact us – We shall come straight back to you. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="sales-professional">
                     <img class="img-responsive alignLeft" src="/assets/images/SP.jpg"/><p class="textRed">Leading the sales force is not always the most rewarding job.
<p>You are held accountable when it goes wrong and not always recognised when it goes well. The tireless drive for consistency and equilibrium in forecasting and delivery is a challenge.</p>
<p>You have the time consuming discussions with marketing, regulatory affairs, IT, BI, production/manufacturing and other vital yet sometimes frustrating department heads,  not to mention the ‘old guard’ within your own team that are change resistant.</p>
<p>The key to success is an energised engaged sales force that has the right skillset, toolset and mind set to articulate the value proposition to the right people at the right time in the right way.</p>
<ul><li>Sales force automation can help, and is only as good as the people operating the systems. A pipeline /CRM tool only gets good Intel out if good Intel is put in. </li>
<li>Visibility of daily weekly activity that gives your leaders insight but does not detract from sales functionality is important to you.</li>
<li>An account development plan is often forgotten if it is not made into a living document by the people reviewing and coaching the plan.</li>
<li>Soft skills’ training is essential and will only work if the roles and responsibilities of sales people and sales managers is clearly defined, and clearly coached on a weekly basis.</li>
<li>An engaged CEO will help – how do you influence him/her and the other executives in a way that enables sales strategy?</li></ol>
<p>Sales Effectiveness is the heart of NGA’s core competence. It is where we came from and where we manage ourselves as a business. </p>
<p>We practice what we preach and what you see is what you get in terms of partnership. </p>
<p>Why not contact us to learn more about how we might work together? <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                    </div>
                      <div class="tab-pane" id="developing-yourself">
                     <img class="img-responsive alignLeft" src="/assets/images/DY.jpg"/><p class="textRed">It doesn’t matter what stage of your career you are at, there is always the opportunity for personal development.
<p>In fact, you might as well replace (or intertwine) the word ‘career’ with the word ‘life’, as they are one and the same in many ways. What you become as a person will have a direct influence in how your life, and then your career develop. </p>
<p>The most common pitfall we come across is that people with busy lives overlook the need for deep reflection on what they do, how they do it and even what they think about. They find themselves wishing for things to be better rather than wishing for more skills. They all too easily complain about their environment but forget to complain about themselves.</p>
<p>At NGA we believe people are capable of the most incredible development given the right opportunity. We believe people care strongly about autonomy, mastery and purpose, and we know that if you have the right mind-set, with the right toolbox, and the right skillset, incredible things will happen.</p>
<p>Check out our <a href="index.php?id=6">Personal Development Members Club</a> and discover what you are capable off!</p>
<p>Or  drop us a mail? – We shall come straight back to you.
 <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p> 
                    </div>  
                  </div>                  
                </div>
            </div>
        </div>
    </div>
   <!-- WHO WE ARE BOX END--> 
   
    <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->

   
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		<ul class="footer_nav"><li class="first active"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->
    <!-- jQuery (necessary for Bootstrap\'s JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Parallax with stellar.js-->
    <script src="/assets/js/parallax.js"></script>
	<script>
    $(function(){
        $.stellar({
            horizontalScrolling: false,
            verticalOffset: -100
        });
    });
    </script>    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/banner.js"></script>
  </body>
</html>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]' => '<ul class="nav"><li class="first active"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>',
    '[[~8]]' => 'index.php?id=8',
    '[[*Heading_title]]' => 'Getting better at what you do every day',
    '[[$Home_header]]' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         <ul class="nav"><li class="first active"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
         </div> </div>  
	<header id="main-header">
		<img id="header-banner" src="/assets/images/banner.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="index.php?id=8" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="http://nga.loyaltymatters.co.uk/"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             <ul class="nav"><li class="first active"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">Getting better at what you do every day </h1>
				<div class="img_man"><img src="/assets/images/man.png" class="img-responsive"/></div>
            </div>            
            <!-- Page Headline Start-->
        </div>

    </header>
    <!-- HEADER END->
    <!-- //Who are we// Block Start-->
    <div class="block_whoarewe">
    	<div class="container">
                <div class="col-lg-9 col-md-9 col-sm-8 description_text">Whether you are facing opportunities or challenges you’ve come to the right place</div>
                <div class="col-lg-3 col-md-3 col-sm-4"><a href="index.php?id=8" class="whoarewe_button"><i class="fa fa-hand-o-right"></i> Who we are</a></div> 
        </div>   
    </div>               
    <!-- //Who are we// Block End-->	',
    '[[~6]]' => 'index.php?id=6',
    '[[$tabbed_content]]' => '   <!-- WHO WE ARE BOX START-->
  	<div class="block_whoareyou">
    	<div class="container">
        	<div class="col-lg-4 col-md-4 col-sm-3  col-xs-2"><hr/></div>
            <div class="col-lg-4 col-md-4 col-sm-6  col-xs-8"><div class="panel-heading">Are you...</div></div>
            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-2"><hr/></div>
        </div>
        <div class="container">
        	<div class="col-lg-12">
                <div class="hidden-lg hidden-md hidden-sm"><div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          A Senior Executive?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/SE.jpg"/><p class="textRed">There is no doubt that as the world begins to transition out of global recession, a business leaders’ focus returns to growth. 
</p> 
    
   <p>The key questions are around how can you defend and grow your current markets whilst driving an innovative expansion strategy that secures top and bottom line?</p>
<p>If you are going to succeed, leadership across multiple levels of the organisation is critical. Everyone must know what they are doing, why they are doing it, and what it means for the organisation. If you had asked the parking attendant at NASA in the 1970’s “What do you do here?” His answer would have been simple… “We put people on the Moon.”</p>
<p>Does everyone in your organisation have such a clear understanding of the Vison, The Mission and the Values that get you all out of bed in the morning? </p>
<p>Does everyone in your organisation live by the ethos of100% accountability and zero excuses for not giving their best each and every day?</p>
<p>Is the story so compelling that you can attract and retain the right talent?</p>
<p>Is the story so compelling that you can seamlessly manage mergers and acquisitions?</p>
<p>Driving transition and leading cultural and organisational change is what NGA do best. Our whole reason for being is to help motivated accountable people secure growth whilst living to their corporate and personal values. </p>
<p>It’s what gets us out of bed in the morning!</p>
<p>If you would like to talk to other executives that have experienced working with NGA then contact us and we will put you in touch. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
         An MD / owner?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/MD.jpg"/><p class="textRed">You are unique!
</p> 


<p>Why? Well according to Forbes, 8 out of 10 entrepreneurs fail before they reach 18 months of being in business; and further research shows that nearly half of the 20% don’t make it past 5 years.</p>
<p>You however are different, and perhaps you are now in a place where it is time to invest in yourself, your leadership team and your people. </p>
<p>Developing your talent as you move from an ‘all hands on deck’ approach to a more structured process driven way of working is always a challenge. </p>
<p>Integrating new people, systems and processes can be difficult as you wrestle with the balance of what made you great as a start up with what will make you great as a sustainable business.</p>
<p>Whether consulting on process design, coaching and training your people managers, or working with your sales force to drive sales, NGA will take the time to understand your situation and provide the support you need for sustainable growth.</p>
<p>We are also able to give you a mirror to look in and a sounding board to challenge you with!</p>
<p>Contact us; we will come straight back to you.<br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          A HR or L&D Professional?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/HR.jpg"/><p class="textRed"> Modern business leaders see the value in HR and L&D departments, not just as a nice to have, but as a need to have in order to realise corporate goals. 
</p> 
    

<p>By utilising tools like business process redesign, organisational redesign, competency model development, and learning and development programmes, it is possible to effectively align human endeavour in the workplace with vision, mission and strategy.</p>
<p>The value of people is fully understood, and often times L&D have a reporting line to HR with the senior HRO sitting alongside the CEO and the rest of the executives.</p>
<p>That said your challenges may still be having a loud and large enough share of voice to influence wider operations within your business; It might still be a challenge to secure budget, and then identify tangible return on investment for your projects, and it is often a challenge to make sure initiatives have real teeth, enabling strategy implementation and not perceived as just tick box exercises.</p>
<p>NGA specialise in supporting HR/L&D functions in two ways</p>
<ol><li>You have done the analysis internally and want a partner to co-design and deliver a programme. During that process you want to be pushed back on and challenged in a way that develops your thinking and elaborates the solution.</li>
<li>You know you could get better at something and need some support in identifying the route cause and creating a sustainable solution to capitalise on the opportunity.</li></ol>
<p>We will help you deliver on your objectives, with tangible ROI, staged KPI metrics to measure impact, and regular feedback routes as the norm.</p>
<p>To find out more, contact us – We shall come straight back to you. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
          A Sales Leader?
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/SP.jpg"/><p class="textRed">Leading the sales force is not always the most rewarding job.
<p>You are held accountable when it goes wrong and not always recognised when it goes well. The tireless drive for consistency and equilibrium in forecasting and delivery is a challenge.</p>
<p>You have the time consuming discussions with marketing, regulatory affairs, IT, BI, production/manufacturing and other vital yet sometimes frustrating department heads,  not to mention the ‘old guard’ within your own team that are change resistant.</p>
<p>The key to success is an energised engaged sales force that has the right skillset, toolset and mind set to articulate the value proposition to the right people at the right time in the right way.</p>
<ul><li>Sales force automation can help, and is only as good as the people operating the systems. A pipeline /CRM tool only gets good Intel out if good Intel is put in. </li>
<li>Visibility of daily weekly activity that gives your leaders insight but does not detract from sales functionality is important to you.</li>
<li>An account development plan is often forgotten if it is not made into a living document by the people reviewing and coaching the plan.</li>
<li>Soft skills’ training is essential and will only work if the roles and responsibilities of sales people and sales managers is clearly defined, and clearly coached on a weekly basis.</li>
<li>An engaged CEO will help – how do you influence him/her and the other executives in a way that enables sales strategy?</li></ol>
<p>Sales Effectiveness is the heart of NGA’s core competence. It is where we came from and where we manage ourselves as a business. </p>
<p>We practice what we preach and what you see is what you get in terms of partnership. </p>
<p>Why not contact us to learn more about how we might work together? <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
          Developing yourself?
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/DY.jpg"/><p class="textRed">It doesn’t matter what stage of your career you are at, there is always the opportunity for personal development.
<p>In fact, you might as well replace (or intertwine) the word ‘career’ with the word ‘life’, as they are one and the same in many ways. What you become as a person will have a direct influence in how your life, and then your career develop. </p>
<p>The most common pitfall we come across is that people with busy lives overlook the need for deep reflection on what they do, how they do it and even what they think about. They find themselves wishing for things to be better rather than wishing for more skills. They all too easily complain about their environment but forget to complain about themselves.</p>
<p>At NGA we believe people are capable of the most incredible development given the right opportunity. We believe people care strongly about autonomy, mastery and purpose, and we know that if you have the right mind-set, with the right toolbox, and the right skillset, incredible things will happen.</p>
<p>Check out our <a href=“index.php?id=6”>Personal Development Members Club</a> and discover what you are capable off!</p>
<p>Or  drop us a mail? – We shall come straight back to you.
 <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
</div></div>
            	<!-- Nav tabs -->
                <div class="hidden-xs">
                <ul class="nav-tabs" role="tablist">
                  <li class="active"><a href="#senior-executive" role="tab" data-toggle="tab">A Senior<br/>
Executive?</a></li>
                  <li><a href="#owner" role="tab" data-toggle="tab">An MD /<br/>
owner?</a></li>
                  <li><a href="#hr-professional" role="tab" data-toggle="tab">HR or L&D <br/>
Professional?</a></li>
                  <li><a href="#sales-professional" role="tab" data-toggle="tab">A Sales <br/>
Leader?</a></li>
                  <li><a href="#developing-yourself" role="tab" data-toggle="tab">Developing <br/>
yourself?</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                      <div class="tab-pane active" id="senior-executive">
                      <img class="img-responsive alignLeft" src="/assets/images/SE.jpg"/><p class="textRed">There is no doubt that as the world begins to transition out of global recession, a business leaders’ focus returns to growth. 
</p> 
    
   <p>The key questions are around how can you defend and grow your current markets whilst driving an innovative expansion strategy that secures top and bottom line?</p>
<p>If you are going to succeed, leadership across multiple levels of the organisation is critical. Everyone must know what they are doing, why they are doing it, and what it means for the organisation. If you had asked the parking attendant at NASA in the 1970’s “What do you do here?” His answer would have been simple… “We put people on the Moon.”</p>
<p>Does everyone in your organisation have such a clear understanding of the Vison, The Mission and the Values that get you all out of bed in the morning? </p>
<p>Does everyone in your organisation live by the ethos of100% accountability and zero excuses for not giving their best each and every day?</p>
<p>Is the story so compelling that you can attract and retain the right talent?</p>
<p>Is the story so compelling that you can seamlessly manage mergers and acquisitions?</p>
<p>Driving transition and leading cultural and organisational change is what NGA do best. Our whole reason for being is to help motivated accountable people secure growth whilst living to their corporate and personal values. </p>
<p>It’s what gets us out of bed in the morning!</p>
<p>If you would like to talk to other executives that have experienced working with NGA then contact us and we will put you in touch. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="owner">
                      <img class="img-responsive alignLeft" src="/assets/images/MD.jpg"/><p class="textRed">You are unique!
</p> 


<p>Why? Well according to Forbes, 8 out of 10 entrepreneurs fail before they reach 18 months of being in business; and further research shows that nearly half of the 20% don’t make it past 5 years.</p>
<p>You however are different, and perhaps you are now in a place where it is time to invest in yourself, your leadership team and your people. </p>
<p>Developing your talent as you move from an ‘all hands on deck’ approach to a more structured process driven way of working is always a challenge. </p>
<p>Integrating new people, systems and processes can be difficult as you wrestle with the balance of what made you great as a start up with what will make you great as a sustainable business.</p>
<p>Whether consulting on process design, coaching and training your people managers, or working with your sales force to drive sales, NGA will take the time to understand your situation and provide the support you need for sustainable growth.</p>
<p>We are also able to give you a mirror to look in and a sounding board to challenge you with!</p>
<p>Contact us; we will come straight back to you.<br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="hr-professional">
                      <img class="img-responsive alignLeft" src="/assets/images/HR.jpg"/><p class="textRed"> Modern business leaders see the value in HR and L&D departments, not just as a nice to have, but as a need to have in order to realise corporate goals. 
</p> 
    

<p>By utilising tools like business process redesign, organisational redesign, competency model development, and learning and development programmes, it is possible to effectively align human endeavour in the workplace with vision, mission and strategy.</p>
<p>The value of people is fully understood, and often times L&D have a reporting line to HR with the senior HRO sitting alongside the CEO and the rest of the executives.</p>
<p>That said your challenges may still be having a loud and large enough share of voice to influence wider operations within your business; It might still be a challenge to secure budget, and then identify tangible return on investment for your projects, and it is often a challenge to make sure initiatives have real teeth, enabling strategy implementation and not perceived as just tick box exercises.</p>
<p>NGA specialise in supporting HR/L&D functions in two ways</p>
<ol><li>You have done the analysis internally and want a partner to co-design and deliver a programme. During that process you want to be pushed back on and challenged in a way that develops your thinking and elaborates the solution.</li>
<li>You know you could get better at something and need some support in identifying the route cause and creating a sustainable solution to capitalise on the opportunity.</li></ol>
<p>We will help you deliver on your objectives, with tangible ROI, staged KPI metrics to measure impact, and regular feedback routes as the norm.</p>
<p>To find out more, contact us – We shall come straight back to you. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="sales-professional">
                     <img class="img-responsive alignLeft" src="/assets/images/SP.jpg"/><p class="textRed">Leading the sales force is not always the most rewarding job.
<p>You are held accountable when it goes wrong and not always recognised when it goes well. The tireless drive for consistency and equilibrium in forecasting and delivery is a challenge.</p>
<p>You have the time consuming discussions with marketing, regulatory affairs, IT, BI, production/manufacturing and other vital yet sometimes frustrating department heads,  not to mention the ‘old guard’ within your own team that are change resistant.</p>
<p>The key to success is an energised engaged sales force that has the right skillset, toolset and mind set to articulate the value proposition to the right people at the right time in the right way.</p>
<ul><li>Sales force automation can help, and is only as good as the people operating the systems. A pipeline /CRM tool only gets good Intel out if good Intel is put in. </li>
<li>Visibility of daily weekly activity that gives your leaders insight but does not detract from sales functionality is important to you.</li>
<li>An account development plan is often forgotten if it is not made into a living document by the people reviewing and coaching the plan.</li>
<li>Soft skills’ training is essential and will only work if the roles and responsibilities of sales people and sales managers is clearly defined, and clearly coached on a weekly basis.</li>
<li>An engaged CEO will help – how do you influence him/her and the other executives in a way that enables sales strategy?</li></ol>
<p>Sales Effectiveness is the heart of NGA’s core competence. It is where we came from and where we manage ourselves as a business. </p>
<p>We practice what we preach and what you see is what you get in terms of partnership. </p>
<p>Why not contact us to learn more about how we might work together? <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                    </div>
                      <div class="tab-pane" id="developing-yourself">
                     <img class="img-responsive alignLeft" src="/assets/images/DY.jpg"/><p class="textRed">It doesn’t matter what stage of your career you are at, there is always the opportunity for personal development.
<p>In fact, you might as well replace (or intertwine) the word ‘career’ with the word ‘life’, as they are one and the same in many ways. What you become as a person will have a direct influence in how your life, and then your career develop. </p>
<p>The most common pitfall we come across is that people with busy lives overlook the need for deep reflection on what they do, how they do it and even what they think about. They find themselves wishing for things to be better rather than wishing for more skills. They all too easily complain about their environment but forget to complain about themselves.</p>
<p>At NGA we believe people are capable of the most incredible development given the right opportunity. We believe people care strongly about autonomy, mastery and purpose, and we know that if you have the right mind-set, with the right toolbox, and the right skillset, incredible things will happen.</p>
<p>Check out our <a href="index.php?id=6">Personal Development Members Club</a> and discover what you are capable off!</p>
<p>Or  drop us a mail? – We shall come straight back to you.
 <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p> 
                    </div>  
                  </div>                  
                </div>
            </div>
        </div>
    </div>
   <!-- WHO WE ARE BOX END--> ',
    '[[$Services]]' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
    '[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]' => '<ul class="footer_nav"><li class="first active"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>',
    '[[$footer]]' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		<ul class="footer_nav"><li class="first active"><a href="http://nga.loyaltymatters.co.uk/" title="Home" >Home</a></li>
<li><a href="index.php?id=3" title="Who we are" >Who we are</a></li>
<li><a href="index.php?id=4" title="Our services" >Our services</a></li>
<li><a href="index.php?id=6" title="Personal Development Club" >Personal Development Club</a></li>
<li class="last"><a href="index.php?id=7" title="Contact us" >Contact us</a></li>
</ul>
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'Home_header' => 
      array (
        'fields' => 
        array (
          'id' => 13,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Home_header',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
         </div> </div>  
	<header id="main-header">
		<img id="header-banner" src="/assets/images/banner.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="[[~8]]" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="[[++site_url]]"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">[[*Heading_title]] </h1>
				<div class="img_man"><img src="/assets/images/man.png" class="img-responsive"/></div>
            </div>            
            <!-- Page Headline Start-->
        </div>

    </header>
    <!-- HEADER END->
    <!-- //Who are we// Block Start-->
    <div class="block_whoarewe">
    	<div class="container">
                <div class="col-lg-9 col-md-9 col-sm-8 description_text">Whether you are facing opportunities or challenges you’ve come to the right place</div>
                <div class="col-lg-3 col-md-3 col-sm-4"><a href="[[~8]]" class="whoarewe_button"><i class="fa fa-hand-o-right"></i> Who we are</a></div> 
        </div>   
    </div>               
    <!-- //Who are we// Block End-->	',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '	<!-- HEADER START-->
        <div class="collapse navbar-collapse"> <div class="hidden-md hidden-sm hidden-lg ">	
         [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
         </div> </div>  
	<header id="main-header">
		<img id="header-banner" src="/assets/images/banner.jpg" alt="">

    	<div class="container">
        	<!-- Partner Login Start-->
        	<div class="col-lg-3 col-lg-offset-9">
            	<div class="floatRight"><a href="[[~8]]" class="areyoumember"><i class="fa fa-user"></i> Are you member? Login here</a></div>
            </div>
            <!-- Partner Login End-->
        </div>

		<div class="container">
            <!-- Logo & Menu Start-->
        	<div class="col-lg-3 col-sm-3 col-md-3"><a href="[[++site_url]]"><img class="img-responsive" src="/assets/images/logo.png"/></a></div>
            <div class="col-lg-9 col-md-9 col-sm-9">
            	<!--<ul class="nav">
                	<li class="first active"><a href="#">Home</a>
                    <li><a href="#">Who are you?</a>
                    <li><a href="#">Who are we?</a>
                    <li><a href="#">Member Benefits</a>
                    <li><a href="#">Member Login</a>
                    <li class="last"><a href="#">Contact us</a>
                </ul>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse" id="navbar-toggle-id">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
			 <div class="hidden-xs">	
             [[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
			 </div>
            </div>
            <!-- Logo & Menu End-->
         </div>
		 <div class="container">
            <!-- Page Headline Start-->
        	<div class="col-lg-12">
            	<h1 class="home">[[*Heading_title]] </h1>
				<div class="img_man"><img src="/assets/images/man.png" class="img-responsive"/></div>
            </div>            
            <!-- Page Headline Start-->
        </div>

    </header>
    <!-- HEADER END->
    <!-- //Who are we// Block Start-->
    <div class="block_whoarewe">
    	<div class="container">
                <div class="col-lg-9 col-md-9 col-sm-8 description_text">Whether you are facing opportunities or challenges you’ve come to the right place</div>
                <div class="col-lg-3 col-md-3 col-sm-4"><a href="[[~8]]" class="whoarewe_button"><i class="fa fa-hand-o-right"></i> Who we are</a></div> 
        </div>   
    </div>               
    <!-- //Who are we// Block End-->	',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'tabbed_content' => 
      array (
        'fields' => 
        array (
          'id' => 10,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'tabbed_content',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '   <!-- WHO WE ARE BOX START-->
  	<div class="block_whoareyou">
    	<div class="container">
        	<div class="col-lg-4 col-md-4 col-sm-3  col-xs-2"><hr/></div>
            <div class="col-lg-4 col-md-4 col-sm-6  col-xs-8"><div class="panel-heading">Are you...</div></div>
            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-2"><hr/></div>
        </div>
        <div class="container">
        	<div class="col-lg-12">
                <div class="hidden-lg hidden-md hidden-sm"><div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          A Senior Executive?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/SE.jpg"/><p class="textRed">There is no doubt that as the world begins to transition out of global recession, a business leaders’ focus returns to growth. 
</p> 
    
   <p>The key questions are around how can you defend and grow your current markets whilst driving an innovative expansion strategy that secures top and bottom line?</p>
<p>If you are going to succeed, leadership across multiple levels of the organisation is critical. Everyone must know what they are doing, why they are doing it, and what it means for the organisation. If you had asked the parking attendant at NASA in the 1970’s “What do you do here?” His answer would have been simple… “We put people on the Moon.”</p>
<p>Does everyone in your organisation have such a clear understanding of the Vison, The Mission and the Values that get you all out of bed in the morning? </p>
<p>Does everyone in your organisation live by the ethos of100% accountability and zero excuses for not giving their best each and every day?</p>
<p>Is the story so compelling that you can attract and retain the right talent?</p>
<p>Is the story so compelling that you can seamlessly manage mergers and acquisitions?</p>
<p>Driving transition and leading cultural and organisational change is what NGA do best. Our whole reason for being is to help motivated accountable people secure growth whilst living to their corporate and personal values. </p>
<p>It’s what gets us out of bed in the morning!</p>
<p>If you would like to talk to other executives that have experienced working with NGA then contact us and we will put you in touch. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
         An MD / owner?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/MD.jpg"/><p class="textRed">You are unique!
</p> 


<p>Why? Well according to Forbes, 8 out of 10 entrepreneurs fail before they reach 18 months of being in business; and further research shows that nearly half of the 20% don’t make it past 5 years.</p>
<p>You however are different, and perhaps you are now in a place where it is time to invest in yourself, your leadership team and your people. </p>
<p>Developing your talent as you move from an ‘all hands on deck’ approach to a more structured process driven way of working is always a challenge. </p>
<p>Integrating new people, systems and processes can be difficult as you wrestle with the balance of what made you great as a start up with what will make you great as a sustainable business.</p>
<p>Whether consulting on process design, coaching and training your people managers, or working with your sales force to drive sales, NGA will take the time to understand your situation and provide the support you need for sustainable growth.</p>
<p>We are also able to give you a mirror to look in and a sounding board to challenge you with!</p>
<p>Contact us; we will come straight back to you.<br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          A HR or L&D Professional?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/HR.jpg"/><p class="textRed"> Modern business leaders see the value in HR and L&D departments, not just as a nice to have, but as a need to have in order to realise corporate goals. 
</p> 
    

<p>By utilising tools like business process redesign, organisational redesign, competency model development, and learning and development programmes, it is possible to effectively align human endeavour in the workplace with vision, mission and strategy.</p>
<p>The value of people is fully understood, and often times L&D have a reporting line to HR with the senior HRO sitting alongside the CEO and the rest of the executives.</p>
<p>That said your challenges may still be having a loud and large enough share of voice to influence wider operations within your business; It might still be a challenge to secure budget, and then identify tangible return on investment for your projects, and it is often a challenge to make sure initiatives have real teeth, enabling strategy implementation and not perceived as just tick box exercises.</p>
<p>NGA specialise in supporting HR/L&D functions in two ways</p>
<ol><li>You have done the analysis internally and want a partner to co-design and deliver a programme. During that process you want to be pushed back on and challenged in a way that develops your thinking and elaborates the solution.</li>
<li>You know you could get better at something and need some support in identifying the route cause and creating a sustainable solution to capitalise on the opportunity.</li></ol>
<p>We will help you deliver on your objectives, with tangible ROI, staged KPI metrics to measure impact, and regular feedback routes as the norm.</p>
<p>To find out more, contact us – We shall come straight back to you. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
          A Sales Leader?
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/SP.jpg"/><p class="textRed">Leading the sales force is not always the most rewarding job.
<p>You are held accountable when it goes wrong and not always recognised when it goes well. The tireless drive for consistency and equilibrium in forecasting and delivery is a challenge.</p>
<p>You have the time consuming discussions with marketing, regulatory affairs, IT, BI, production/manufacturing and other vital yet sometimes frustrating department heads,  not to mention the ‘old guard’ within your own team that are change resistant.</p>
<p>The key to success is an energised engaged sales force that has the right skillset, toolset and mind set to articulate the value proposition to the right people at the right time in the right way.</p>
<ul><li>Sales force automation can help, and is only as good as the people operating the systems. A pipeline /CRM tool only gets good Intel out if good Intel is put in. </li>
<li>Visibility of daily weekly activity that gives your leaders insight but does not detract from sales functionality is important to you.</li>
<li>An account development plan is often forgotten if it is not made into a living document by the people reviewing and coaching the plan.</li>
<li>Soft skills’ training is essential and will only work if the roles and responsibilities of sales people and sales managers is clearly defined, and clearly coached on a weekly basis.</li>
<li>An engaged CEO will help – how do you influence him/her and the other executives in a way that enables sales strategy?</li></ol>
<p>Sales Effectiveness is the heart of NGA’s core competence. It is where we came from and where we manage ourselves as a business. </p>
<p>We practice what we preach and what you see is what you get in terms of partnership. </p>
<p>Why not contact us to learn more about how we might work together? <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
          Developing yourself?
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/DY.jpg"/><p class="textRed">It doesn’t matter what stage of your career you are at, there is always the opportunity for personal development.
<p>In fact, you might as well replace (or intertwine) the word ‘career’ with the word ‘life’, as they are one and the same in many ways. What you become as a person will have a direct influence in how your life, and then your career develop. </p>
<p>The most common pitfall we come across is that people with busy lives overlook the need for deep reflection on what they do, how they do it and even what they think about. They find themselves wishing for things to be better rather than wishing for more skills. They all too easily complain about their environment but forget to complain about themselves.</p>
<p>At NGA we believe people are capable of the most incredible development given the right opportunity. We believe people care strongly about autonomy, mastery and purpose, and we know that if you have the right mind-set, with the right toolbox, and the right skillset, incredible things will happen.</p>
<p>Check out our <a href=“[[~6]]”>Personal Development Members Club</a> and discover what you are capable off!</p>
<p>Or  drop us a mail? – We shall come straight back to you.
 <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
</div></div>
            	<!-- Nav tabs -->
                <div class="hidden-xs">
                <ul class="nav-tabs" role="tablist">
                  <li class="active"><a href="#senior-executive" role="tab" data-toggle="tab">A Senior<br/>
Executive?</a></li>
                  <li><a href="#owner" role="tab" data-toggle="tab">An MD /<br/>
owner?</a></li>
                  <li><a href="#hr-professional" role="tab" data-toggle="tab">HR or L&D <br/>
Professional?</a></li>
                  <li><a href="#sales-professional" role="tab" data-toggle="tab">A Sales <br/>
Leader?</a></li>
                  <li><a href="#developing-yourself" role="tab" data-toggle="tab">Developing <br/>
yourself?</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                      <div class="tab-pane active" id="senior-executive">
                      <img class="img-responsive alignLeft" src="/assets/images/SE.jpg"/><p class="textRed">There is no doubt that as the world begins to transition out of global recession, a business leaders’ focus returns to growth. 
</p> 
    
   <p>The key questions are around how can you defend and grow your current markets whilst driving an innovative expansion strategy that secures top and bottom line?</p>
<p>If you are going to succeed, leadership across multiple levels of the organisation is critical. Everyone must know what they are doing, why they are doing it, and what it means for the organisation. If you had asked the parking attendant at NASA in the 1970’s “What do you do here?” His answer would have been simple… “We put people on the Moon.”</p>
<p>Does everyone in your organisation have such a clear understanding of the Vison, The Mission and the Values that get you all out of bed in the morning? </p>
<p>Does everyone in your organisation live by the ethos of100% accountability and zero excuses for not giving their best each and every day?</p>
<p>Is the story so compelling that you can attract and retain the right talent?</p>
<p>Is the story so compelling that you can seamlessly manage mergers and acquisitions?</p>
<p>Driving transition and leading cultural and organisational change is what NGA do best. Our whole reason for being is to help motivated accountable people secure growth whilst living to their corporate and personal values. </p>
<p>It’s what gets us out of bed in the morning!</p>
<p>If you would like to talk to other executives that have experienced working with NGA then contact us and we will put you in touch. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="owner">
                      <img class="img-responsive alignLeft" src="/assets/images/MD.jpg"/><p class="textRed">You are unique!
</p> 


<p>Why? Well according to Forbes, 8 out of 10 entrepreneurs fail before they reach 18 months of being in business; and further research shows that nearly half of the 20% don’t make it past 5 years.</p>
<p>You however are different, and perhaps you are now in a place where it is time to invest in yourself, your leadership team and your people. </p>
<p>Developing your talent as you move from an ‘all hands on deck’ approach to a more structured process driven way of working is always a challenge. </p>
<p>Integrating new people, systems and processes can be difficult as you wrestle with the balance of what made you great as a start up with what will make you great as a sustainable business.</p>
<p>Whether consulting on process design, coaching and training your people managers, or working with your sales force to drive sales, NGA will take the time to understand your situation and provide the support you need for sustainable growth.</p>
<p>We are also able to give you a mirror to look in and a sounding board to challenge you with!</p>
<p>Contact us; we will come straight back to you.<br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="hr-professional">
                      <img class="img-responsive alignLeft" src="/assets/images/HR.jpg"/><p class="textRed"> Modern business leaders see the value in HR and L&D departments, not just as a nice to have, but as a need to have in order to realise corporate goals. 
</p> 
    

<p>By utilising tools like business process redesign, organisational redesign, competency model development, and learning and development programmes, it is possible to effectively align human endeavour in the workplace with vision, mission and strategy.</p>
<p>The value of people is fully understood, and often times L&D have a reporting line to HR with the senior HRO sitting alongside the CEO and the rest of the executives.</p>
<p>That said your challenges may still be having a loud and large enough share of voice to influence wider operations within your business; It might still be a challenge to secure budget, and then identify tangible return on investment for your projects, and it is often a challenge to make sure initiatives have real teeth, enabling strategy implementation and not perceived as just tick box exercises.</p>
<p>NGA specialise in supporting HR/L&D functions in two ways</p>
<ol><li>You have done the analysis internally and want a partner to co-design and deliver a programme. During that process you want to be pushed back on and challenged in a way that develops your thinking and elaborates the solution.</li>
<li>You know you could get better at something and need some support in identifying the route cause and creating a sustainable solution to capitalise on the opportunity.</li></ol>
<p>We will help you deliver on your objectives, with tangible ROI, staged KPI metrics to measure impact, and regular feedback routes as the norm.</p>
<p>To find out more, contact us – We shall come straight back to you. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="sales-professional">
                     <img class="img-responsive alignLeft" src="/assets/images/SP.jpg"/><p class="textRed">Leading the sales force is not always the most rewarding job.
<p>You are held accountable when it goes wrong and not always recognised when it goes well. The tireless drive for consistency and equilibrium in forecasting and delivery is a challenge.</p>
<p>You have the time consuming discussions with marketing, regulatory affairs, IT, BI, production/manufacturing and other vital yet sometimes frustrating department heads,  not to mention the ‘old guard’ within your own team that are change resistant.</p>
<p>The key to success is an energised engaged sales force that has the right skillset, toolset and mind set to articulate the value proposition to the right people at the right time in the right way.</p>
<ul><li>Sales force automation can help, and is only as good as the people operating the systems. A pipeline /CRM tool only gets good Intel out if good Intel is put in. </li>
<li>Visibility of daily weekly activity that gives your leaders insight but does not detract from sales functionality is important to you.</li>
<li>An account development plan is often forgotten if it is not made into a living document by the people reviewing and coaching the plan.</li>
<li>Soft skills’ training is essential and will only work if the roles and responsibilities of sales people and sales managers is clearly defined, and clearly coached on a weekly basis.</li>
<li>An engaged CEO will help – how do you influence him/her and the other executives in a way that enables sales strategy?</li></ol>
<p>Sales Effectiveness is the heart of NGA’s core competence. It is where we came from and where we manage ourselves as a business. </p>
<p>We practice what we preach and what you see is what you get in terms of partnership. </p>
<p>Why not contact us to learn more about how we might work together? <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                    </div>
                      <div class="tab-pane" id="developing-yourself">
                     <img class="img-responsive alignLeft" src="/assets/images/DY.jpg"/><p class="textRed">It doesn’t matter what stage of your career you are at, there is always the opportunity for personal development.
<p>In fact, you might as well replace (or intertwine) the word ‘career’ with the word ‘life’, as they are one and the same in many ways. What you become as a person will have a direct influence in how your life, and then your career develop. </p>
<p>The most common pitfall we come across is that people with busy lives overlook the need for deep reflection on what they do, how they do it and even what they think about. They find themselves wishing for things to be better rather than wishing for more skills. They all too easily complain about their environment but forget to complain about themselves.</p>
<p>At NGA we believe people are capable of the most incredible development given the right opportunity. We believe people care strongly about autonomy, mastery and purpose, and we know that if you have the right mind-set, with the right toolbox, and the right skillset, incredible things will happen.</p>
<p>Check out our <a href="[[~6]]">Personal Development Members Club</a> and discover what you are capable off!</p>
<p>Or  drop us a mail? – We shall come straight back to you.
 <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p> 
                    </div>  
                  </div>                  
                </div>
            </div>
        </div>
    </div>
   <!-- WHO WE ARE BOX END--> ',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '   <!-- WHO WE ARE BOX START-->
  	<div class="block_whoareyou">
    	<div class="container">
        	<div class="col-lg-4 col-md-4 col-sm-3  col-xs-2"><hr/></div>
            <div class="col-lg-4 col-md-4 col-sm-6  col-xs-8"><div class="panel-heading">Are you...</div></div>
            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-2"><hr/></div>
        </div>
        <div class="container">
        	<div class="col-lg-12">
                <div class="hidden-lg hidden-md hidden-sm"><div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          A Senior Executive?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/SE.jpg"/><p class="textRed">There is no doubt that as the world begins to transition out of global recession, a business leaders’ focus returns to growth. 
</p> 
    
   <p>The key questions are around how can you defend and grow your current markets whilst driving an innovative expansion strategy that secures top and bottom line?</p>
<p>If you are going to succeed, leadership across multiple levels of the organisation is critical. Everyone must know what they are doing, why they are doing it, and what it means for the organisation. If you had asked the parking attendant at NASA in the 1970’s “What do you do here?” His answer would have been simple… “We put people on the Moon.”</p>
<p>Does everyone in your organisation have such a clear understanding of the Vison, The Mission and the Values that get you all out of bed in the morning? </p>
<p>Does everyone in your organisation live by the ethos of100% accountability and zero excuses for not giving their best each and every day?</p>
<p>Is the story so compelling that you can attract and retain the right talent?</p>
<p>Is the story so compelling that you can seamlessly manage mergers and acquisitions?</p>
<p>Driving transition and leading cultural and organisational change is what NGA do best. Our whole reason for being is to help motivated accountable people secure growth whilst living to their corporate and personal values. </p>
<p>It’s what gets us out of bed in the morning!</p>
<p>If you would like to talk to other executives that have experienced working with NGA then contact us and we will put you in touch. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
         An MD / owner?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/MD.jpg"/><p class="textRed">You are unique!
</p> 


<p>Why? Well according to Forbes, 8 out of 10 entrepreneurs fail before they reach 18 months of being in business; and further research shows that nearly half of the 20% don’t make it past 5 years.</p>
<p>You however are different, and perhaps you are now in a place where it is time to invest in yourself, your leadership team and your people. </p>
<p>Developing your talent as you move from an ‘all hands on deck’ approach to a more structured process driven way of working is always a challenge. </p>
<p>Integrating new people, systems and processes can be difficult as you wrestle with the balance of what made you great as a start up with what will make you great as a sustainable business.</p>
<p>Whether consulting on process design, coaching and training your people managers, or working with your sales force to drive sales, NGA will take the time to understand your situation and provide the support you need for sustainable growth.</p>
<p>We are also able to give you a mirror to look in and a sounding board to challenge you with!</p>
<p>Contact us; we will come straight back to you.<br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          A HR or L&D Professional?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/HR.jpg"/><p class="textRed"> Modern business leaders see the value in HR and L&D departments, not just as a nice to have, but as a need to have in order to realise corporate goals. 
</p> 
    

<p>By utilising tools like business process redesign, organisational redesign, competency model development, and learning and development programmes, it is possible to effectively align human endeavour in the workplace with vision, mission and strategy.</p>
<p>The value of people is fully understood, and often times L&D have a reporting line to HR with the senior HRO sitting alongside the CEO and the rest of the executives.</p>
<p>That said your challenges may still be having a loud and large enough share of voice to influence wider operations within your business; It might still be a challenge to secure budget, and then identify tangible return on investment for your projects, and it is often a challenge to make sure initiatives have real teeth, enabling strategy implementation and not perceived as just tick box exercises.</p>
<p>NGA specialise in supporting HR/L&D functions in two ways</p>
<ol><li>You have done the analysis internally and want a partner to co-design and deliver a programme. During that process you want to be pushed back on and challenged in a way that develops your thinking and elaborates the solution.</li>
<li>You know you could get better at something and need some support in identifying the route cause and creating a sustainable solution to capitalise on the opportunity.</li></ol>
<p>We will help you deliver on your objectives, with tangible ROI, staged KPI metrics to measure impact, and regular feedback routes as the norm.</p>
<p>To find out more, contact us – We shall come straight back to you. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
          A Sales Leader?
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/SP.jpg"/><p class="textRed">Leading the sales force is not always the most rewarding job.
<p>You are held accountable when it goes wrong and not always recognised when it goes well. The tireless drive for consistency and equilibrium in forecasting and delivery is a challenge.</p>
<p>You have the time consuming discussions with marketing, regulatory affairs, IT, BI, production/manufacturing and other vital yet sometimes frustrating department heads,  not to mention the ‘old guard’ within your own team that are change resistant.</p>
<p>The key to success is an energised engaged sales force that has the right skillset, toolset and mind set to articulate the value proposition to the right people at the right time in the right way.</p>
<ul><li>Sales force automation can help, and is only as good as the people operating the systems. A pipeline /CRM tool only gets good Intel out if good Intel is put in. </li>
<li>Visibility of daily weekly activity that gives your leaders insight but does not detract from sales functionality is important to you.</li>
<li>An account development plan is often forgotten if it is not made into a living document by the people reviewing and coaching the plan.</li>
<li>Soft skills’ training is essential and will only work if the roles and responsibilities of sales people and sales managers is clearly defined, and clearly coached on a weekly basis.</li>
<li>An engaged CEO will help – how do you influence him/her and the other executives in a way that enables sales strategy?</li></ol>
<p>Sales Effectiveness is the heart of NGA’s core competence. It is where we came from and where we manage ourselves as a business. </p>
<p>We practice what we preach and what you see is what you get in terms of partnership. </p>
<p>Why not contact us to learn more about how we might work together? <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
          Developing yourself?
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse">
      <div class="panel-body">
        <img class="img-responsive alignLeft" src="/assets/images/DY.jpg"/><p class="textRed">It doesn’t matter what stage of your career you are at, there is always the opportunity for personal development.
<p>In fact, you might as well replace (or intertwine) the word ‘career’ with the word ‘life’, as they are one and the same in many ways. What you become as a person will have a direct influence in how your life, and then your career develop. </p>
<p>The most common pitfall we come across is that people with busy lives overlook the need for deep reflection on what they do, how they do it and even what they think about. They find themselves wishing for things to be better rather than wishing for more skills. They all too easily complain about their environment but forget to complain about themselves.</p>
<p>At NGA we believe people are capable of the most incredible development given the right opportunity. We believe people care strongly about autonomy, mastery and purpose, and we know that if you have the right mind-set, with the right toolbox, and the right skillset, incredible things will happen.</p>
<p>Check out our <a href=“[[~6]]”>Personal Development Members Club</a> and discover what you are capable off!</p>
<p>Or  drop us a mail? – We shall come straight back to you.
 <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
      </div>
    </div>
  </div>
</div></div>
            	<!-- Nav tabs -->
                <div class="hidden-xs">
                <ul class="nav-tabs" role="tablist">
                  <li class="active"><a href="#senior-executive" role="tab" data-toggle="tab">A Senior<br/>
Executive?</a></li>
                  <li><a href="#owner" role="tab" data-toggle="tab">An MD /<br/>
owner?</a></li>
                  <li><a href="#hr-professional" role="tab" data-toggle="tab">HR or L&D <br/>
Professional?</a></li>
                  <li><a href="#sales-professional" role="tab" data-toggle="tab">A Sales <br/>
Leader?</a></li>
                  <li><a href="#developing-yourself" role="tab" data-toggle="tab">Developing <br/>
yourself?</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                      <div class="tab-pane active" id="senior-executive">
                      <img class="img-responsive alignLeft" src="/assets/images/SE.jpg"/><p class="textRed">There is no doubt that as the world begins to transition out of global recession, a business leaders’ focus returns to growth. 
</p> 
    
   <p>The key questions are around how can you defend and grow your current markets whilst driving an innovative expansion strategy that secures top and bottom line?</p>
<p>If you are going to succeed, leadership across multiple levels of the organisation is critical. Everyone must know what they are doing, why they are doing it, and what it means for the organisation. If you had asked the parking attendant at NASA in the 1970’s “What do you do here?” His answer would have been simple… “We put people on the Moon.”</p>
<p>Does everyone in your organisation have such a clear understanding of the Vison, The Mission and the Values that get you all out of bed in the morning? </p>
<p>Does everyone in your organisation live by the ethos of100% accountability and zero excuses for not giving their best each and every day?</p>
<p>Is the story so compelling that you can attract and retain the right talent?</p>
<p>Is the story so compelling that you can seamlessly manage mergers and acquisitions?</p>
<p>Driving transition and leading cultural and organisational change is what NGA do best. Our whole reason for being is to help motivated accountable people secure growth whilst living to their corporate and personal values. </p>
<p>It’s what gets us out of bed in the morning!</p>
<p>If you would like to talk to other executives that have experienced working with NGA then contact us and we will put you in touch. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="owner">
                      <img class="img-responsive alignLeft" src="/assets/images/MD.jpg"/><p class="textRed">You are unique!
</p> 


<p>Why? Well according to Forbes, 8 out of 10 entrepreneurs fail before they reach 18 months of being in business; and further research shows that nearly half of the 20% don’t make it past 5 years.</p>
<p>You however are different, and perhaps you are now in a place where it is time to invest in yourself, your leadership team and your people. </p>
<p>Developing your talent as you move from an ‘all hands on deck’ approach to a more structured process driven way of working is always a challenge. </p>
<p>Integrating new people, systems and processes can be difficult as you wrestle with the balance of what made you great as a start up with what will make you great as a sustainable business.</p>
<p>Whether consulting on process design, coaching and training your people managers, or working with your sales force to drive sales, NGA will take the time to understand your situation and provide the support you need for sustainable growth.</p>
<p>We are also able to give you a mirror to look in and a sounding board to challenge you with!</p>
<p>Contact us; we will come straight back to you.<br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="hr-professional">
                      <img class="img-responsive alignLeft" src="/assets/images/HR.jpg"/><p class="textRed"> Modern business leaders see the value in HR and L&D departments, not just as a nice to have, but as a need to have in order to realise corporate goals. 
</p> 
    

<p>By utilising tools like business process redesign, organisational redesign, competency model development, and learning and development programmes, it is possible to effectively align human endeavour in the workplace with vision, mission and strategy.</p>
<p>The value of people is fully understood, and often times L&D have a reporting line to HR with the senior HRO sitting alongside the CEO and the rest of the executives.</p>
<p>That said your challenges may still be having a loud and large enough share of voice to influence wider operations within your business; It might still be a challenge to secure budget, and then identify tangible return on investment for your projects, and it is often a challenge to make sure initiatives have real teeth, enabling strategy implementation and not perceived as just tick box exercises.</p>
<p>NGA specialise in supporting HR/L&D functions in two ways</p>
<ol><li>You have done the analysis internally and want a partner to co-design and deliver a programme. During that process you want to be pushed back on and challenged in a way that develops your thinking and elaborates the solution.</li>
<li>You know you could get better at something and need some support in identifying the route cause and creating a sustainable solution to capitalise on the opportunity.</li></ol>
<p>We will help you deliver on your objectives, with tangible ROI, staged KPI metrics to measure impact, and regular feedback routes as the norm.</p>
<p>To find out more, contact us – We shall come straight back to you. <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                      </div>
                      <div class="tab-pane" id="sales-professional">
                     <img class="img-responsive alignLeft" src="/assets/images/SP.jpg"/><p class="textRed">Leading the sales force is not always the most rewarding job.
<p>You are held accountable when it goes wrong and not always recognised when it goes well. The tireless drive for consistency and equilibrium in forecasting and delivery is a challenge.</p>
<p>You have the time consuming discussions with marketing, regulatory affairs, IT, BI, production/manufacturing and other vital yet sometimes frustrating department heads,  not to mention the ‘old guard’ within your own team that are change resistant.</p>
<p>The key to success is an energised engaged sales force that has the right skillset, toolset and mind set to articulate the value proposition to the right people at the right time in the right way.</p>
<ul><li>Sales force automation can help, and is only as good as the people operating the systems. A pipeline /CRM tool only gets good Intel out if good Intel is put in. </li>
<li>Visibility of daily weekly activity that gives your leaders insight but does not detract from sales functionality is important to you.</li>
<li>An account development plan is often forgotten if it is not made into a living document by the people reviewing and coaching the plan.</li>
<li>Soft skills’ training is essential and will only work if the roles and responsibilities of sales people and sales managers is clearly defined, and clearly coached on a weekly basis.</li>
<li>An engaged CEO will help – how do you influence him/her and the other executives in a way that enables sales strategy?</li></ol>
<p>Sales Effectiveness is the heart of NGA’s core competence. It is where we came from and where we manage ourselves as a business. </p>
<p>We practice what we preach and what you see is what you get in terms of partnership. </p>
<p>Why not contact us to learn more about how we might work together? <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p>
                    </div>
                      <div class="tab-pane" id="developing-yourself">
                     <img class="img-responsive alignLeft" src="/assets/images/DY.jpg"/><p class="textRed">It doesn’t matter what stage of your career you are at, there is always the opportunity for personal development.
<p>In fact, you might as well replace (or intertwine) the word ‘career’ with the word ‘life’, as they are one and the same in many ways. What you become as a person will have a direct influence in how your life, and then your career develop. </p>
<p>The most common pitfall we come across is that people with busy lives overlook the need for deep reflection on what they do, how they do it and even what they think about. They find themselves wishing for things to be better rather than wishing for more skills. They all too easily complain about their environment but forget to complain about themselves.</p>
<p>At NGA we believe people are capable of the most incredible development given the right opportunity. We believe people care strongly about autonomy, mastery and purpose, and we know that if you have the right mind-set, with the right toolbox, and the right skillset, incredible things will happen.</p>
<p>Check out our <a href="[[~6]]">Personal Development Members Club</a> and discover what you are capable off!</p>
<p>Or  drop us a mail? – We shall come straight back to you.
 <br/> <br/><button name="link" class="btn btn-primary btn-lg btn-red">Contact us</button></p> 
                    </div>  
                  </div>                  
                </div>
            </div>
        </div>
    </div>
   <!-- WHO WE ARE BOX END--> ',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'Services' => 
      array (
        'fields' => 
        array (
          'id' => 9,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Services',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => ' <!-- SERVICES BOX START-->
   <div class="block_services"> 
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="panel-heading">Our Services</div></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><hr/></div>
    </div>     	
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-consulting.png"/> 
                <h3>NGA PDM Club</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Consulting</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-meeting.png"/> 
                <h3>NGA Training</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
        	<div class="content-box">
            	<img class="img-responsive" src="/assets/images/icon-training.png"/> 
                <h3>NGA Coaching</h3>
                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor interdum mi in pellentesque. Nulla ac lacinia felis.<br><br><a class="learnmore" href="#">Learn more</a></p>
            </div>        
        </div>
     </div>	
   </div>
   <!-- SERVICES BOX START-->',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'footer' => 
      array (
        'fields' => 
        array (
          'id' => 7,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'cache_type' => 0,
          'snippet' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '
   <!-- Footer Start-->
   <div class="footer">
     <div class="container">
     	<div class="col-lg-3 col-md-3 col-sm-3">
		[[Wayfinder? &startId=`0`&level=`2`&outerTpl=`msjnav-outer`&outerClass=`footer_nav`&rowTpl=`rowItem`&innerTpl=`msjnav-inner`]]
        	<!-- <ul class="footer_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul> -->
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Mailing List</h4>
            Sign up if you would like to receive offers and information.<br/>
            
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Follow us</h4>
            Connect to us via social media for up to the minute news.<br/>
            <div class="social_buttons">
            <img class="img-responsive" src="/assets/images/fb.png"/><img class="img-responsive" src="/assets/images/tw.png"/><img class="img-responsive" src="/assets/images/yt.png"/>
            <img class="img-responsive" src="/assets/images/ln.png"/>
            <img class="img-responsive" src="/assets/images/gp.png"/>
            </div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3">
        	<h4>Get in Touch</h4>
            104 Station Parade<br/>Harrogate HG1 1HQ<br/>Tel: +44 (0) 7900 564 879<br/>Email: info@nickgirlingassociates.com
        </div>
     </div>
     
     <div class="container">
     	<div class="col-lg-12">Privacy  |  Terms & Conditions</div>
     </div>
   </div>
   <!-- Footer End-->',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'Wayfinder' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Wayfinder',
          'description' => 'Wayfinder for MODx Revolution 2.0.0-beta-5 and later.',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * Wayfinder Snippet to build site navigation menus
 *
 * Totally refactored from original DropMenu nav builder to make it easier to
 * create custom navigation by using chunks as output templates. By using
 * templates, many of the paramaters are no longer needed for flexible output
 * including tables, unordered- or ordered-lists (ULs or OLs), definition lists
 * (DLs) or in any other format you desire.
 *
 * @version 2.1.1-beta5
 * @author Garry Nutting (collabpad.com)
 * @author Kyle Jaebker (muddydogpaws.com)
 * @author Ryan Thrash (modx.com)
 * @author Shaun McCormick (modx.com)
 * @author Jason Coward (modx.com)
 *
 * @example [[Wayfinder? &startId=`0`]]
 *
 * @var modX $modx
 * @var array $scriptProperties
 * 
 * @package wayfinder
 */
$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');

/* include a custom config file if specified */
if (isset($scriptProperties[\'config\'])) {
    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';
} else {
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';
}
if (file_exists($scriptProperties[\'config\'])) {
    include $scriptProperties[\'config\'];
}

/* include wayfinder class */
include_once $wayfinder_base.\'wayfinder.class.php\';
if (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {
    return \'error: Wayfinder class not found\';
}
$wf = new Wayfinder($modx,$scriptProperties);

/* get user class definitions
 * TODO: eventually move these into config parameters */
$wf->_css = array(
    \'first\' => isset($firstClass) ? $firstClass : \'\',
    \'last\' => isset($lastClass) ? $lastClass : \'last\',
    \'here\' => isset($hereClass) ? $hereClass : \'active\',
    \'parent\' => isset($parentClass) ? $parentClass : \'\',
    \'row\' => isset($rowClass) ? $rowClass : \'\',
    \'outer\' => isset($outerClass) ? $outerClass : \'\',
    \'inner\' => isset($innerClass) ? $innerClass : \'\',
    \'level\' => isset($levelClass) ? $levelClass: \'\',
    \'self\' => isset($selfClass) ? $selfClass : \'\',
    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'
);

/* get user templates
 * TODO: eventually move these into config parameters */
$wf->_templates = array(
    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',
    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',
    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',
    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',
    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',
    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',
    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',
    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',
    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',
    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',
    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'
);

/* process Wayfinder */
$output = $wf->run();
if ($wf->_config[\'debug\']) {
    $output .= $wf->renderDebugOutput();
}

/* output results */
if ($wf->_config[\'ph\']) {
    $modx->setPlaceholder($wf->_config[\'ph\'],$output);
} else {
    return $output;
}',
          'locked' => false,
          'properties' => 
          array (
            'level' => 
            array (
              'name' => 'level',
              'desc' => 'prop_wayfinder.level_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Depth (number of levels) to build the menu from. 0 goes through all levels.',
              'area' => '',
              'area_trans' => '',
            ),
            'includeDocs' => 
            array (
              'name' => 'includeDocs',
              'desc' => 'prop_wayfinder.includeDocs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Acts as a filter and will limit the output to only the documents specified in this parameter. The startId is still required.',
              'area' => '',
              'area_trans' => '',
            ),
            'excludeDocs' => 
            array (
              'name' => 'excludeDocs',
              'desc' => 'prop_wayfinder.excludeDocs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Acts as a filter and will remove the documents specified in this parameter from the output. The startId is still required.',
              'area' => '',
              'area_trans' => '',
            ),
            'contexts' => 
            array (
              'name' => 'contexts',
              'desc' => 'prop_wayfinder.contexts_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Specify the contexts for the Resources that will be loaded in this menu. Useful when used with startId at 0 to show all first-level items. Note: This will increase load times a bit, but if you set cacheResults to 1, that will offset the load time.',
              'area' => '',
              'area_trans' => '',
            ),
            'cacheResults' => 
            array (
              'name' => 'cacheResults',
              'desc' => 'prop_wayfinder.cacheResults_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Cache the generated menu to the MODX Resource cache. Setting this to 1 will speed up the loading of your menus.',
              'area' => '',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'prop_wayfinder.cacheTime_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 3600,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The number of seconds to store the cached menu, if cacheResults is 1. Set to 0 to store indefinitely until cache is manually cleared.',
              'area' => '',
              'area_trans' => '',
            ),
            'ph' => 
            array (
              'name' => 'ph',
              'desc' => 'prop_wayfinder.ph_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'To display send the output of Wayfinder to a placeholder set the ph parameter equal to the name of the desired placeholder. All output including the debugging (if on) will be sent to the placeholder specified.',
              'area' => '',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'prop_wayfinder.debug_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'With the debug parameter set to 1, Wayfinder will output information on how each Resource was processed.',
              'area' => '',
              'area_trans' => '',
            ),
            'ignoreHidden' => 
            array (
              'name' => 'ignoreHidden',
              'desc' => 'prop_wayfinder.ignoreHidden_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The ignoreHidden parameter allows Wayfinder to ignore the display in menu flag that can be set for each document. With this parameter set to 1, all Resources will be displayed regardless of the Display in Menu flag.',
              'area' => '',
              'area_trans' => '',
            ),
            'hideSubMenus' => 
            array (
              'name' => 'hideSubMenus',
              'desc' => 'prop_wayfinder.hideSubMenus_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The hideSubMenus parameter will remove all non-active submenus from the Wayfinder output if set to 1. This parameter only works if multiple levels are being displayed.',
              'area' => '',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'prop_wayfinder.useWeblinkUrl_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => ' If WebLinks are used in the output, Wayfinder will output the link specified in the WebLink instead of the normal MODx link. To use the standard display of WebLinks (like any other Resource) set this to 0.',
              'area' => '',
              'area_trans' => '',
            ),
            'fullLink' => 
            array (
              'name' => 'fullLink',
              'desc' => 'prop_wayfinder.fullLink_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'If set to 1, will display the entire, absolute URL in the link. (It is recommended to use scheme instead.)',
              'area' => '',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'prop_wayfinder.scheme_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'prop_wayfinder.relative',
                  'value' => '',
                  'name' => 'Relative',
                ),
                1 => 
                array (
                  'text' => 'prop_wayfinder.absolute',
                  'value' => 'abs',
                  'name' => 'Absolute',
                ),
                2 => 
                array (
                  'text' => 'prop_wayfinder.full',
                  'value' => 'full',
                  'name' => 'Full',
                ),
              ),
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Determines how URLs are generated for each link. Set to "abs" to show the absolute URL, "full" to show the full URL, and blank to use the relative URL. Defaults to relative.',
              'area' => '',
              'area_trans' => '',
            ),
            'sortOrder' => 
            array (
              'name' => 'sortOrder',
              'desc' => 'prop_wayfinder.sortOrder_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'prop_wayfinder.ascending',
                  'value' => 'ASC',
                  'name' => 'Ascending',
                ),
                1 => 
                array (
                  'text' => 'prop_wayfinder.descending',
                  'value' => 'DESC',
                  'name' => 'Descending',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Allows the menu to be sorted in either ascending or descending order.',
              'area' => '',
              'area_trans' => '',
            ),
            'sortBy' => 
            array (
              'name' => 'sortBy',
              'desc' => 'prop_wayfinder.sortBy_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'menuindex',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Sorts the output by any of the Resource fields on a level by level basis. This means that each submenu will be sorted independently of all other submenus at the same level. Random will sort the output differently every time the page is loaded if the snippet is called uncached.',
              'area' => '',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'prop_wayfinder.limit_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Causes Wayfinder to only process the number of items specified per level.',
              'area' => '',
              'area_trans' => '',
            ),
            'cssTpl' => 
            array (
              'name' => 'cssTpl',
              'desc' => 'prop_wayfinder.cssTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This parameter allows for a chunk containing a link to a style sheet or style information to be inserted into the head section of the generated page.',
              'area' => '',
              'area_trans' => '',
            ),
            'jsTpl' => 
            array (
              'name' => 'jsTpl',
              'desc' => 'prop_wayfinder.jsTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This parameter allows for a chunk containing some Javascript to be inserted into the head section of the generated page.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowIdPrefix' => 
            array (
              'name' => 'rowIdPrefix',
              'desc' => 'prop_wayfinder.rowIdPrefix_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'If set, Wayfinder will replace the id placeholder with a unique id consisting of the specified prefix plus the Resource id.',
              'area' => '',
              'area_trans' => '',
            ),
            'textOfLinks' => 
            array (
              'name' => 'textOfLinks',
              'desc' => 'prop_wayfinder.textOfLinks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'menutitle',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This field will be inserted into the linktext placeholder.',
              'area' => '',
              'area_trans' => '',
            ),
            'titleOfLinks' => 
            array (
              'name' => 'titleOfLinks',
              'desc' => 'prop_wayfinder.titleOfLinks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'pagetitle',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This field will be inserted into the linktitle placeholder.',
              'area' => '',
              'area_trans' => '',
            ),
            'displayStart' => 
            array (
              'name' => 'displayStart',
              'desc' => 'prop_wayfinder.displayStart_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Show the document as referenced by startId in the menu.',
              'area' => '',
              'area_trans' => '',
            ),
            'firstClass' => 
            array (
              'name' => 'firstClass',
              'desc' => 'prop_wayfinder.firstClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'first',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the first item at a given menu level.',
              'area' => '',
              'area_trans' => '',
            ),
            'lastClass' => 
            array (
              'name' => 'lastClass',
              'desc' => 'prop_wayfinder.lastClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'last',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the last item at a given menu level.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereClass' => 
            array (
              'name' => 'hereClass',
              'desc' => 'prop_wayfinder.hereClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'active',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the items showing where you are, all the way up the chain.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentClass' => 
            array (
              'name' => 'parentClass',
              'desc' => 'prop_wayfinder.parentClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for menu items that are a container and have children.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowClass' => 
            array (
              'name' => 'rowClass',
              'desc' => 'prop_wayfinder.rowClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class denoting each output row.',
              'area' => '',
              'area_trans' => '',
            ),
            'outerClass' => 
            array (
              'name' => 'outerClass',
              'desc' => 'prop_wayfinder.outerClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the outer template.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerClass' => 
            array (
              'name' => 'innerClass',
              'desc' => 'prop_wayfinder.innerClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the inner template.',
              'area' => '',
              'area_trans' => '',
            ),
            'levelClass' => 
            array (
              'name' => 'levelClass',
              'desc' => 'prop_wayfinder.levelClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class denoting every output row level. The level number will be added to the specified class (level1, level2, level3 etc if you specified "level").',
              'area' => '',
              'area_trans' => '',
            ),
            'selfClass' => 
            array (
              'name' => 'selfClass',
              'desc' => 'prop_wayfinder.selfClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the current item.',
              'area' => '',
              'area_trans' => '',
            ),
            'webLinkClass' => 
            array (
              'name' => 'webLinkClass',
              'desc' => 'prop_wayfinder.webLinkClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for weblink items.',
              'area' => '',
              'area_trans' => '',
            ),
            'outerTpl' => 
            array (
              'name' => 'outerTpl',
              'desc' => 'prop_wayfinder.outerTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the outer most container; if not included, a string including "<ul>[[+wf.wrapper]]</ul>" is assumed.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowTpl' => 
            array (
              'name' => 'rowTpl',
              'desc' => 'prop_wayfinder.rowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the regular row items.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentRowTpl' => 
            array (
              'name' => 'parentRowTpl',
              'desc' => 'prop_wayfinder.parentRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for any Resource that is a container and has children. Remember the [wf.wrapper] placeholder to output the children documents.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentRowHereTpl' => 
            array (
              'name' => 'parentRowHereTpl',
              'desc' => 'prop_wayfinder.parentRowHereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource if it is a container and has children. Remember the [wf.wrapper] placeholder to output the children documents.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereTpl' => 
            array (
              'name' => 'hereTpl',
              'desc' => 'prop_wayfinder.hereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerTpl' => 
            array (
              'name' => 'innerTpl',
              'desc' => 'prop_wayfinder.innerTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for each submenu. If no innerTpl is specified the outerTpl is used in its place.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerRowTpl' => 
            array (
              'name' => 'innerRowTpl',
              'desc' => 'prop_wayfinder.innerRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the row items in a subfolder.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerHereTpl' => 
            array (
              'name' => 'innerHereTpl',
              'desc' => 'prop_wayfinder.innerHereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource if it is in a subfolder.',
              'area' => '',
              'area_trans' => '',
            ),
            'activeParentRowTpl' => 
            array (
              'name' => 'activeParentRowTpl',
              'desc' => 'prop_wayfinder.activeParentRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for items that are containers, have children and are currently active in the tree.',
              'area' => '',
              'area_trans' => '',
            ),
            'categoryFoldersTpl' => 
            array (
              'name' => 'categoryFoldersTpl',
              'desc' => 'prop_wayfinder.categoryFoldersTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for category folders. Category folders are determined by setting the template to blank or by setting the link attributes field to rel="category".',
              'area' => '',
              'area_trans' => '',
            ),
            'startItemTpl' => 
            array (
              'name' => 'startItemTpl',
              'desc' => 'prop_wayfinder.startItemTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the start item, if enabled via the &displayStart parameter. Note: the default template shows the start item but does not link it. If you do not need a link, a class can be applied to the default template using the parameter &firstClass=`className`.',
              'area' => '',
              'area_trans' => '',
            ),
            'permissions' => 
            array (
              'name' => 'permissions',
              'desc' => 'prop_wayfinder.permissions_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'list',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Will check for a permission on the Resource. Defaults to "list" - set to blank to skip normal permissions checks.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereId' => 
            array (
              'name' => 'hereId',
              'desc' => 'prop_wayfinder.hereId_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. If set, will change the "here" Resource to this ID. Defaults to the currently active Resource.',
              'area' => '',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'prop_wayfinder.where_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. A JSON object for where conditions for all items selected in the menu.',
              'area' => '',
              'area_trans' => '',
            ),
            'templates' => 
            array (
              'name' => 'templates',
              'desc' => 'prop_wayfinder.templates_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. A comma-separated list of Template IDs to restrict selected Resources to.',
              'area' => '',
              'area_trans' => '',
            ),
            'previewUnpublished' => 
            array (
              'name' => 'previewUnpublished',
              'desc' => 'prop_wayfinder.previewunpublished_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. If set to Yes, if you are logged into the mgr and have the view_unpublished permission, it will allow previewing of unpublished resources in your menus in the front-end.',
              'area' => '',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * Wayfinder Snippet to build site navigation menus
 *
 * Totally refactored from original DropMenu nav builder to make it easier to
 * create custom navigation by using chunks as output templates. By using
 * templates, many of the paramaters are no longer needed for flexible output
 * including tables, unordered- or ordered-lists (ULs or OLs), definition lists
 * (DLs) or in any other format you desire.
 *
 * @version 2.1.1-beta5
 * @author Garry Nutting (collabpad.com)
 * @author Kyle Jaebker (muddydogpaws.com)
 * @author Ryan Thrash (modx.com)
 * @author Shaun McCormick (modx.com)
 * @author Jason Coward (modx.com)
 *
 * @example [[Wayfinder? &startId=`0`]]
 *
 * @var modX $modx
 * @var array $scriptProperties
 * 
 * @package wayfinder
 */
$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');

/* include a custom config file if specified */
if (isset($scriptProperties[\'config\'])) {
    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';
} else {
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';
}
if (file_exists($scriptProperties[\'config\'])) {
    include $scriptProperties[\'config\'];
}

/* include wayfinder class */
include_once $wayfinder_base.\'wayfinder.class.php\';
if (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {
    return \'error: Wayfinder class not found\';
}
$wf = new Wayfinder($modx,$scriptProperties);

/* get user class definitions
 * TODO: eventually move these into config parameters */
$wf->_css = array(
    \'first\' => isset($firstClass) ? $firstClass : \'\',
    \'last\' => isset($lastClass) ? $lastClass : \'last\',
    \'here\' => isset($hereClass) ? $hereClass : \'active\',
    \'parent\' => isset($parentClass) ? $parentClass : \'\',
    \'row\' => isset($rowClass) ? $rowClass : \'\',
    \'outer\' => isset($outerClass) ? $outerClass : \'\',
    \'inner\' => isset($innerClass) ? $innerClass : \'\',
    \'level\' => isset($levelClass) ? $levelClass: \'\',
    \'self\' => isset($selfClass) ? $selfClass : \'\',
    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'
);

/* get user templates
 * TODO: eventually move these into config parameters */
$wf->_templates = array(
    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',
    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',
    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',
    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',
    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',
    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',
    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',
    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',
    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',
    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',
    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'
);

/* process Wayfinder */
$output = $wf->run();
if ($wf->_config[\'debug\']) {
    $output .= $wf->renderDebugOutput();
}

/* output results */
if ($wf->_config[\'ph\']) {
    $modx->setPlaceholder($wf->_config[\'ph\'],$output);
} else {
    return $output;
}',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'Heading_title' => 
      array (
        'fields' => 
        array (
          'id' => 14,
          'source' => 0,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'Heading_title',
          'caption' => 'Banner Text',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
    ),
  ),
);