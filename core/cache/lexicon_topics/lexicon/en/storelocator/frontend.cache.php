<?php  return array (
  'storelocator' => 'Store Locator',
  'storelocator.submit' => 'Search',
  'storelocator.clear' => 'Clear',
  'storelocator.address_not_found' => 'Address not found, please try again',
  'storelocator.address' => 'Enter your address',
  'storelocator.radius' => 'Radius',
  'storelocator.click_to_view' => 'Click here to view',
  'storelocator.noresults' => 'No stores found, please try a different radius.',
);